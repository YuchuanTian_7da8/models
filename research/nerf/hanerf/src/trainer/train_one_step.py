# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""Training wrapper."""

from math import sqrt
from mindspore import nn


class TrainOneStepCell(nn.Cell):

    def __init__(self,
                 train_config,
                 loss,
                 network,
                 optimizer):
        super(TrainOneStepCell, self).__init__()
        self.train_config = train_config
        self.loss = loss
        self.network = network
        self.optimizer = optimizer

    def construct(self, *inputs):
        rays, ts, rgbs, whole_img, _, uv_sample = inputs
        rgbs_shape = rgbs.shape[0]
        h = int(sqrt(rgbs_shape))
        w = int(sqrt(rgbs_shape))
        results = self.network(rays, ts, whole_img, w, h, uv_sample)
        loss = self.loss(results, rgbs,
                         self.train_config, self.optimizer.global_step)

        return loss
