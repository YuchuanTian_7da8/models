# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/rover-xingyu/Ha-NeRF
# ============================================================================
"""Script for prepare data cache."""

import argparse
import os
import pickle
import numpy as np

from src.data.phototourism_mask_grid_sample import PhotoTourismDataset


def get_opts():
    parser = argparse.ArgumentParser()

    parser.add_argument('--root_dir', type=str, required=True,
                        help='root directory of dataset')
    parser.add_argument('--img_downscale', type=int, default=1,
                        help='how much to downscale the '
                             'images for phototourism dataset')

    return parser.parse_args()


if __name__ == '__main__':
    args = get_opts()
    os.makedirs(os.path.join(args.root_dir, 'cache'), exist_ok=True)
    print(f'Preparing cache for scale {args.img_downscale}...')
    dataset = PhotoTourismDataset(args.root_dir, 'train', args.img_downscale)
    # save img ids
    with open(os.path.join(args.root_dir, 'cache/img_ids.pkl'), 'wb') as f:
        pickle.dump(dataset.img_ids, f, pickle.HIGHEST_PROTOCOL)
    # save img paths
    with open(os.path.join(args.root_dir,
                           'cache/image_paths.pkl'), 'wb') as f:
        pickle.dump(dataset.image_paths, f, pickle.HIGHEST_PROTOCOL)
    # save Ks
    with open(os.path.join(args.root_dir,
                           f'cache/Ks{args.img_downscale}.pkl'), 'wb') as f:
        pickle.dump(dataset.Ks, f, pickle.HIGHEST_PROTOCOL)

    # save all_imgs
    with open(os.path.join(args.root_dir,
                           f'cache/all_imgs{8}.pkl'), 'wb') as f:
        pickle.dump(dataset.all_imgs, f, pickle.HIGHEST_PROTOCOL)

    # save scene points
    np.save(os.path.join(args.root_dir, 'cache/xyz_world.npy'),
            dataset.xyz_world)
    # save poses
    np.save(os.path.join(args.root_dir, 'cache/poses.npy'),
            dataset.poses)
    # save near and far bounds
    with open(os.path.join(args.root_dir, 'cache/nears.pkl'), 'wb') as f:
        pickle.dump(dataset.nears, f, pickle.HIGHEST_PROTOCOL)
    with open(os.path.join(args.root_dir, 'cache/fars.pkl'), 'wb') as f:
        pickle.dump(dataset.fars, f, pickle.HIGHEST_PROTOCOL)
    # save rays and rgbs
    np.save(os.path.join(args.root_dir,
                         f'cache/rays{args.img_downscale}.npy'),
            dataset.all_rays)
    np.save(os.path.join(args.root_dir,
                         f'cache/rgbs{args.img_downscale}.npy'),
            dataset.all_rgbs)

    # save all_imgs_wh
    np.save(os.path.join(args.root_dir,
                         f'cache/all_imgs_wh{args.img_downscale}.npy'),
            dataset.all_imgs_wh)

    print(f"Data cache saved to {os.path.join(args.root_dir, 'cache')} !")
