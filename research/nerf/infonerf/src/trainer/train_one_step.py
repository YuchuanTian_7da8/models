# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/mjmjeong/InfoNeRF
# ============================================================================
"""Training wrapper for downgrading smoothing lambda during epochs."""

from mindspore import nn
import mindspore as ms


class TrainOneStepCellSmoothing(nn.Cell):

    def __init__(self,
                 network,
                 optimizer,
                 train_ds_size,
                 min_smoothing_lambda=None):
        super(TrainOneStepCellSmoothing, self).__init__()
        self.network = network
        self.optimizer = optimizer
        self.train_ds_size = train_ds_size
        self.min_smoothing_lambda = min_smoothing_lambda
        self.maximum = ms.ops.Maximum()

    def construct(self, *inputs):
        step = self.optimizer.global_step / self.train_ds_size
        smoothing_lambda = self.network.smoothing_lambda * \
            self.network.smoothing_rate ** \
            ((step / self.network.smoothing_step_size).int())
        if self.min_smoothing_lambda != -1:
            smoothing_lambda = self.maximum(
                smoothing_lambda, ms.Tensor(self.min_smoothing_lambda)
            )
        loss = self.network(*inputs, smoothing_lambda)
        return loss
