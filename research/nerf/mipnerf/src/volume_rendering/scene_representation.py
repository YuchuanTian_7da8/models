# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/kakaobrain/nerf-factory
# ============================================================================
"""NeRF explicit 3D model scene representation."""

import numpy as np

import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops

from mindspore import jit_class


class NeRFMLP(nn.Cell):
    def __init__(self, min_deg_point,
                 max_deg_point,
                 deg_view,
                 depth: int = 8, width: int = 256,
                 netdepth_condition: int = 1,
                 netwidth_condition: int = 128,
                 input_ch=3, input_ch_view: int = 3,
                 num_rgb_channels=3, num_density_channels: int = 1,
                 skips=4,
                 ):
        super().__init__(auto_prefix=False)
        self.depth = depth
        self.skips = skips
        self.netdepth_condition = netdepth_condition
        self.num_density_channels = num_density_channels
        self.num_rgb_channels = num_rgb_channels

        pos_size = ((max_deg_point - min_deg_point) * 2) * input_ch
        view_pos_size = (deg_view * 2 + 1) * input_ch_view

        # Ops.
        self.concat = ops.Concat(axis=-1)

        # Layers.
        self.relu = nn.ReLU()
        pts_linears = [nn.Dense(pos_size, width, activation='relu')]
        for i in range(depth - 1):
            if i % skips == 0 and i > 0:
                pts_linears.append(nn.Dense(width + pos_size, width, activation='relu'))
            else:
                pts_linears.append(nn.Dense(width, width, activation='relu'))
        self.pts_linears = nn.CellList(pts_linears)

        views_linear = [nn.Dense(width + view_pos_size, netwidth_condition, activation='relu')]
        for _ in range(netdepth_condition - 1):
            views_linear.append(nn.Dense(netwidth_condition, netwidth_condition, activation='relu'))
        self.views_linear = nn.CellList(views_linear)

        self.bottleneck_layer = nn.Dense(width, width)
        self.density_layer = nn.Dense(width, num_density_channels)
        self.rgb_layer = nn.Dense(netwidth_condition, num_rgb_channels)
        self.reshape = ops.Reshape()
        self.tile = ops.Tile()

    def construct(self, x, condition):
        """
        :param x: ms.Tensor, [batch, num_samples, feature]
        :param condition: ms.Tensor, [batch, feature]
        :return:
        """
        num_samples, feat_dim = x.shape[1:]
        x = self.reshape(x, (-1, feat_dim))
        inputs = x
        for i in range(self.depth):
            x = self.pts_linears[i](x)
            if i % self.skips == 0 and i > 0:
                x = self.concat((x, inputs))

        raw_density = self.reshape(self.density_layer(x),
                                   (-1, num_samples, self.num_density_channels)
                                   )
        bottleneck = self.bottleneck_layer(x)

        condition_tiled = self.tile(condition[:, None, :], (1, num_samples, 1))
        condition_tiled = self.reshape(condition_tiled, (-1, condition.shape[-1]))
        x = self.concat([bottleneck, condition_tiled])
        for i in range(self.netdepth_condition):
            x = self.views_linear[i](x)

        raw_rgb = self.reshape(self.rgb_layer(x), (-1, num_samples, self.num_rgb_channels))

        return raw_rgb, raw_density


@jit_class
class PosAndDirEncoder:
    def __init__(self,
                 min_deg_point: int = 0,
                 max_deg_point: int = 16,
                 deg_view: int = 4,
                 append_identity: bool = True,
                 dtype=ms.float32
                 ):
        self.min_deg_point = min_deg_point
        self.max_deg_point = max_deg_point
        self.deg_view = deg_view
        self.append_identity = append_identity
        self.dtype = dtype

        self.cat = ops.Concat(axis=-1)

        self.scales_pos = ms.Tensor(
            [2 ** i for i in range(self.min_deg_point, self.max_deg_point)],
            dtype=self.dtype)
        self.scales_dir = ms.Tensor([2 ** i for i in range(0, self.deg_view)],
                                    dtype=self.dtype)
        self.reshape = ops.Reshape()
        self.sin = ops.Sin()
        self.exp = ops.Exp()

    def integrated_pos_enc(self, samples):
        x, x_cov_diag = samples
        shape = tuple(x.shape[:-1]) + (-1,)
        y = self.reshape(x[..., None, :] * self.scales_pos[:, None], shape)
        y_var = self.reshape(x_cov_diag[..., None, :] *
                             self.scales_pos[:, None] ** 2, shape)
        x_sin = self.cat([y, y + 0.5 * np.pi])
        x_var = self.cat([y_var] * 2)

        res = self.exp(-0.5 * x_var) * self.sin(x_sin)
        return res

    def pos_enc(self, x):
        shape = tuple(x.shape[:-1]) + (-1,)
        xb = self.reshape(x[..., None, :] * self.scales_dir[:, None], shape)
        four_feat = self.sin(self.cat([xb, xb + 0.5 * np.pi]))
        if self.append_identity:
            return self.cat([x] + [four_feat])
        return four_feat

    def pos_enc_mesh(self, x):
        shape = tuple(x.shape[:-1]) + (-1,)
        xb = self.reshape(x[..., None, :] * self.scales_pos[:, None], shape)
        four_feat = self.sin(self.cat([xb, xb + 0.5 * np.pi]))
        return four_feat
