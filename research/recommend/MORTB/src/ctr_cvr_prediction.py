# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
code for predicting CTR and CVR for the dataset
"""
import os
import time

import numpy as np
import pandas as pd

from mindspore import ops
from mindspore.ops import operations as P
from mindspore.ops import composite as C
from mindspore.nn.metrics import Metric
import mindspore.common.dtype as mstype
from mindspore.common.initializer import Uniform, initializer, Normal
from mindspore import nn, ParameterTuple, Parameter
from mindspore.train.callback import Callback
from mindspore.train.callback import ModelCheckpoint, CheckpointConfig
from mindspore import save_checkpoint
from sklearn.metrics import roc_auc_score

np_type = np.float32
ms_type = mstype.float32
ms_type_16 = mstype.float16


def init_method(method, shape, name, max_val=0.01):
    """
    The method of init parameters.

    Args:
        method (str): The method uses to initialize parameter.
        shape (list): The shape of parameter.
        name (str): The name of parameter.
        max_val (float): Max value in parameter when uses 'random' or 'uniform' to initialize parameter.

    Returns:
        Parameter.
    """
    if method in ['random', 'uniform']:
        params = Parameter(initializer(Uniform(max_val), shape, ms_type), name=name)
    elif method == "one":
        params = Parameter(initializer("ones", shape, ms_type), name=name)
    elif method == 'zero':
        params = Parameter(initializer("zeros", shape, ms_type), name=name)
    elif method == "normal":
        params = Parameter(initializer(Normal(max_val), shape, ms_type), name=name)
    return params


class Dataset:
    def __init__(self, data_path, mode, task, infer=False):
        self.task = task
        self.mode = mode
        data_path = os.path.join(data_path, 'criteo_attribution_dataset.preprocess.%s.tsv' % mode)
        self.data = pd.read_csv(data_path, sep='\t')
        if self.task == 'cvr' and not infer:
            self.data = self.data[self.data['ctr_label'] == 1]
        self.data = self.data.reset_index(drop=True).to_numpy()
        print(mode, task, self.data.shape, self.data.dtype)

    def __getitem__(self, item):
        if self.task == 'cvr':
            return self.data[item][1:-2], self.data[item][-1].astype(np.float32)
        return self.data[item][1:-2], self.data[item][-2].astype(np.float32)

    def __len__(self):
        return len(self.data)


class ModelBuilder:
    def __init__(self, args):
        self.args = args
        self.net = LogisticRegression()
        self.test_net = LREvalNet(self.net)
        self.loss_net = NetWithLossCell(self.net, P.SigmoidCrossEntropyWithLogits())
        self.train_net = TrainStepWrap(self.loss_net, lr=self.args.lr)
        self.auc_metric = AUCMetric()

    def get_callback_list(self, model=None, eval_dataset=None):
        """
        Get callbacks which contains checkpoint callback, eval callback and loss callback.

        Args:
            model (Cell): The network is added callback (default=None).
            eval_dataset (Dataset): Dataset for eval (default=None).
        """
        callback_list = []
        if self.args.save_checkpoint:
            config_ck = CheckpointConfig(save_checkpoint_steps=self.args.save_checkpoint_steps,
                                         keep_checkpoint_max=self.args.keep_checkpoint_max)
            ckpt_cb = ModelCheckpoint(prefix=self.args.task,
                                      directory=self.args.model_path,
                                      config=config_ck)
            callback_list.append(ckpt_cb)
        if self.args.eval_callback:
            if model is None:
                raise RuntimeError("train_config.eval_callback is {}; get_callback_list() args model is {}".format(
                    self.args.eval_callback, model))
            if eval_dataset is None:
                raise RuntimeError("train_config.eval_callback is {}; get_callback_list() "
                                   "args eval_dataset is {}".format(self.args.eval_callback, eval_dataset))
            auc_metric = AUCMetric()
            eval_callback = EvalCallBack(model, eval_dataset, auc_metric,
                                         eval_file_path=os.path.join(self.args.log_path,
                                                                     'Eval_' + self.args.task),
                                         model_path=os.path.join(self.args.model_path, self.args.task + '_best.ckpt'),
                                         early_stop_epoch=self.args.early_stop_epoch)
            callback_list.append(eval_callback)
        if self.args.loss_callback:
            loss_callback = LossCallBack(loss_file_path=os.path.join(self.args.log_path,
                                                                     'Loss_' + self.args.task))
            callback_list.append(loss_callback)
        if callback_list:
            return callback_list
        return None


class LogisticRegression(nn.Cell):
    def __init__(self):
        super(LogisticRegression, self).__init__()
        self.embeddings = nn.Embedding(60000, 1)
        self.bias = init_method('zero', [1], name="bias")
        self.reduce_sum = P.ReduceSum(keep_dims=False)

    def construct(self, x):
        return self.reduce_sum(self.embeddings(x).squeeze(-1), 1) + self.bias


class LREvalNet(nn.Cell):
    def __init__(self, network):
        super(LREvalNet, self).__init__(auto_prefix=False)
        self.network = network
        self.sigmoid = P.Sigmoid()

    def construct(self, data, labels):
        pred = self.sigmoid(self.network(data))
        return pred, labels


class NetWithLossCell(nn.Cell):
    def __init__(self, backbone, loss_fn):
        super(NetWithLossCell, self).__init__(auto_prefix=False)
        self.backbone = backbone
        self.loss_fn = loss_fn
        self.reduce_mean = P.ReduceMean()
        self.l2loss = ops.L2Loss()

    def construct(self, data, label):
        out = self.backbone(data)
        return self.reduce_mean(self.loss_fn(out, label))

    def backbone_network(self):
        return self.backbone


class TrainStepWrap(nn.Cell):
    """
    TrainStepWrap definition
    """
    def __init__(self, network, lr, eps=1e-9):
        super(TrainStepWrap, self).__init__(auto_prefix=False)
        self.network = network
        self.network.set_grad()
        self.network.set_train()
        self.weights = ParameterTuple(network.trainable_params())
        self.optimizer = nn.Adam(self.weights, learning_rate=lr, eps=eps)
        self.grad = C.GradOperation(get_by_list=True)

    def construct(self, data, label):
        weights = self.weights
        loss = self.network(data, label)
        grads = self.grad(self.network, weights)(data, label)
        self.optimizer(grads)
        return loss


class AUCMetric(Metric):
    def __init__(self):
        super(AUCMetric, self).__init__()
        self.pred_probs = []
        self.true_labels = []

    def clear(self):
        self.pred_probs = []
        self.true_labels = []

    def update(self, *inputs):
        batch_predict = inputs[0].asnumpy()
        batch_label = inputs[1].asnumpy()
        self.pred_probs.extend(batch_predict.flatten().tolist())
        self.true_labels.extend(batch_label.flatten().tolist())

    def eval(self):
        if len(self.true_labels) != len(self.pred_probs):
            raise RuntimeError('true_labels.size() is not equal to pred_probs.size()')
        auc = roc_auc_score(self.true_labels, self.pred_probs)
        return auc


def add_write(file_path, out_str):
    with open(file_path, 'a+', encoding='utf-8') as file_out:
        file_out.write(out_str + '\n')


class EvalCallBack(Callback):
    """
    Monitor the loss in training.
    If the loss is NAN or INF terminating training.
    Note
        If per_print_times is 0 do not print loss.
    """
    def __init__(self, model, eval_dataset, auc_metric, eval_file_path, model_path, early_stop_epoch):
        super(EvalCallBack, self).__init__()
        self.model = model
        self.eval_dataset = eval_dataset
        self.aucMetric = auc_metric
        self.aucMetric.clear()
        self.eval_file_path = eval_file_path
        self.model_path = model_path
        self.early_stop_epoch = early_stop_epoch
        self.cur_best_auc = 0
        self.cur_best_epoch = 0

    def on_train_epoch_end(self, run_context):
        cb_params = run_context.original_args()
        start_time = time.time()
        out = self.model.eval(self.eval_dataset)
        eval_time = int(time.time() - start_time)
        time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        out_str = "{} EvalCallBack metric{}; eval_time{}s".format(
            time_str, out.values(), eval_time)
        print(out_str)
        if out['auc'] > self.cur_best_auc:
            self.cur_best_auc = out['auc']
            self.cur_best_epoch = cb_params.cur_epoch_num
            save_checkpoint(cb_params.train_network, self.model_path)
        if cb_params.cur_epoch_num - self.cur_best_epoch > self.early_stop_epoch:
            run_context.request_stop()
        add_write(self.eval_file_path, out_str)


class LossCallBack(Callback):
    """
    Monitor the loss in training.
    If the loss is NAN or INF terminating training.
    Note
        If per_print_times is 0 do not print loss.
    Args
        loss_file_path (str) The file absolute path, to save as loss_file;
        per_print_times (int) Print loss every times. Default 1000.
    """
    def __init__(self, loss_file_path, per_print_times=1000):
        super(LossCallBack, self).__init__()
        if not isinstance(per_print_times, int) or per_print_times < 0:
            raise ValueError("print_step must be int and >= 0.")
        self.loss_file_path = loss_file_path
        self._per_print_times = per_print_times

    def on_train_step_end(self, run_context):
        """Monitor the loss in training."""
        cb_params = run_context.original_args()
        loss = cb_params.net_outputs.asnumpy()
        cur_step_in_epoch = (cb_params.cur_step_num - 1) % cb_params.batch_num + 1
        if self._per_print_times != 0 and cur_step_in_epoch % self._per_print_times == 1:
            with open(self.loss_file_path, "a+") as loss_file:
                time_str = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
                loss_file.write("{} epoch: {} step: {}, loss is {}\n".format(
                    time_str, cb_params.cur_epoch_num, cur_step_in_epoch, loss))
            print("epoch: {} step: {}, loss is {}".format(
                cb_params.cur_epoch_num, cur_step_in_epoch, loss))


class TimeMonitor(Callback):
    """
    Time monitor for calculating cost of each epoch.
    Args
        data_size (int) step size of an epoch.
    """
    def __init__(self, data_size):
        super(TimeMonitor, self).__init__()
        self.data_size = data_size

    def on_train_epoch_begin(self, run_context):
        self.epoch_time = time.time()

    def on_train_epoch_end(self, run_context):
        epoch_mseconds = (time.time() - self.epoch_time) * 1000
        per_step_mseconds = epoch_mseconds / self.data_size
        print("epoch time: {0}, per step time: {1}".format(epoch_mseconds, per_step_mseconds), flush=True)

    def on_train_step_begin(self, run_context):
        self.step_time = time.time()

    def on_train_step_end(self, run_context):
        step_mseconds = (time.time() - self.step_time) * 1000
        print(f"step time {step_mseconds}", flush=True)
