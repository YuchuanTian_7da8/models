# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import mindspore as ms
import numpy as np

class Dataset:
    def __init__(self, data, n_question):
        self.data = data
        self.n_question = n_question

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        data = self.data[index]
        question = data['q_ids']
        user_id = data['user_id']
        mask = np.zeros(self.n_question)
        mask[question] = 1
        return user_id, ms.Tensor(mask)
