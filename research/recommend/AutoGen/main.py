#encoding=utf-8
import os
import sys
from mindspore import nn
from mindspore import ops
from mindspore import Tensor, load_checkpoint, load_param_into_net, Parameter
import mindspore
from Movielens import Movielens
from test_matrix import test_one_user
import numpy as np

sys.path.append('/NAS2020/Workspaces/DMGroup/zcx/Huawei/code/handler_dataset/')

seeds = [
    0x0123, 0x4567, 0x3210, 0x7654, 0x89AB, 0xCDEF, 0xBA98, 0xFEDC, 0x0123,
    0x4567, 0x3210, 0x7654, 0x89AB, 0xCDEF, 0xBA98, 0xFEDC
]
backend = 'tf'
os.environ["CUDA_VISIBLE_DEVICES"] = "0"


class DSSM_multi_input_once_for_all(nn.Cell):

    def __init__(self,
                 user_field_num=None,
                 item_field_num=None,
                 user_feature_num=None,
                 item_feature_num=None,
                 embedding_size=None,
                 user_dnn_hidden_unit=None,
                 item_dnn_hidden_unit=None):
        super(DSSM_multi_input_once_for_all, self).__init__()
        item_field_num += 1
        self.user_field_num, self.item_field_num = user_field_num, item_field_num
        self.beta_num = 8
        self.user_v = Parameter(Tensor(
            np.ones((user_feature_num, embedding_size)), mindspore.float32),
                                name='user_v')
        self.item_v = Parameter(Tensor(
            np.ones([item_feature_num, embedding_size]), mindspore.float32),
                                name='item_v')
        self.embedding_size = embedding_size

        self.user_w0 = Parameter(Tensor(
            np.ones([user_field_num * embedding_size,
                     user_dnn_hidden_unit[0]]), mindspore.float32),
                                 name='user_dnn_w_0')
        self.user_b0 = Parameter(Tensor(np.ones([user_dnn_hidden_unit[0]]),
                                        mindspore.float32),
                                 name='user_dnn_b_0')
        self.user_w1 = Parameter(Tensor(
            np.ones([
                user_field_num * embedding_size + user_dnn_hidden_unit[0],
                user_dnn_hidden_unit[1]
            ]), mindspore.float32),
                                 name='user_dnn_w_1')
        self.user_b1 = Parameter(Tensor(np.ones([user_dnn_hidden_unit[1]]),
                                        mindspore.float32),
                                 name='user_dnn_b_1')
        self.user_w2 = Parameter(Tensor(
            np.ones([
                user_field_num * embedding_size + user_dnn_hidden_unit[1],
                user_dnn_hidden_unit[2]
            ]), mindspore.float32),
                                 name='user_dnn_w_2')
        self.user_b2 = Parameter(Tensor(np.ones([user_dnn_hidden_unit[2]]),
                                        mindspore.float32),
                                 name='user_dnn_b_2')
        self.item_w0 = Parameter(Tensor(
            np.ones([item_field_num * embedding_size,
                     item_dnn_hidden_unit[0]]), mindspore.float32),
                                 name='item_dnn_w_0')
        self.item_b0 = Parameter(Tensor(np.ones([item_dnn_hidden_unit[0]]),
                                        mindspore.float32),
                                 name='item_dnn_b_0')
        self.item_w1 = Parameter(Tensor(
            np.ones([
                item_field_num * embedding_size + item_dnn_hidden_unit[0],
                item_dnn_hidden_unit[1]
            ]), mindspore.float32),
                                 name='item_dnn_w_1')
        self.item_b1 = Parameter(Tensor(np.ones([item_dnn_hidden_unit[1]]),
                                        mindspore.float32),
                                 name='item_dnn_b_1')
        self.item_w2 = Parameter(Tensor(
            np.ones([
                item_field_num * embedding_size + item_dnn_hidden_unit[1],
                item_dnn_hidden_unit[2]
            ]), mindspore.float32),
                                 name='item_dnn_w_2')
        self.item_b2 = Parameter(Tensor(np.ones([item_dnn_hidden_unit[2]]),
                                        mindspore.float32),
                                 name='item_dnn_b_2')
        self.user_bn0, self.user_bn1, self.item_bn0, self.item_bn1 = ops.BatchNorm(epsilon=0.001, momentum=0.99), \
                                                                     ops.BatchNorm(epsilon=0.001, momentum=0.99), \
                                                                     ops.BatchNorm(epsilon=0.001, momentum=0.99), \
                                                                     ops.BatchNorm(epsilon=0.001, momentum=0.99)
        self.user_bn0_gamma = Parameter(Tensor(
            np.ones(
                [user_field_num * embedding_size + user_dnn_hidden_unit[0]]),
            mindspore.float32),
                                        name='user_bn0_gamma')
        self.user_bn0_beta = Parameter(Tensor(
            np.ones(
                [user_field_num * embedding_size + user_dnn_hidden_unit[0]]),
            mindspore.float32),
                                       name='user_bn0_beta')
        self.user_bn0_mean = Parameter(Tensor(
            np.ones(
                [user_field_num * embedding_size + user_dnn_hidden_unit[0]]),
            mindspore.float32),
                                       name='user_bn0_mean')
        self.user_bn0_var = Parameter(Tensor(
            np.ones(
                [user_field_num * embedding_size + user_dnn_hidden_unit[0]]),
            mindspore.float32),
                                      name='user_bn0_var')

        self.user_bn1_gamma = Parameter(Tensor(
            np.ones(
                [user_field_num * embedding_size + user_dnn_hidden_unit[1]]),
            mindspore.float32),
                                        name='user_bn1_gamma')
        self.user_bn1_beta = Parameter(Tensor(
            np.ones(
                [user_field_num * embedding_size + user_dnn_hidden_unit[1]]),
            mindspore.float32),
                                       name='user_bn1_beta')
        self.user_bn1_mean = Parameter(Tensor(
            np.ones(
                [user_field_num * embedding_size + user_dnn_hidden_unit[1]]),
            mindspore.float32),
                                       name='user_bn1_mean')
        self.user_bn1_var = Parameter(Tensor(
            np.ones(
                [user_field_num * embedding_size + user_dnn_hidden_unit[1]]),
            mindspore.float32),
                                      name='user_bn1_var')

        self.item_bn0_gamma = Parameter(Tensor(
            np.ones(
                [item_field_num * embedding_size + item_dnn_hidden_unit[0]]),
            mindspore.float32),
                                        name='item_bn0_gamma')
        self.item_bn0_beta = Parameter(Tensor(
            np.ones(
                [item_field_num * embedding_size + item_dnn_hidden_unit[0]]),
            mindspore.float32),
                                       name='item_bn0_beta')
        self.item_bn0_mean = Parameter(Tensor(
            np.ones(
                [item_field_num * embedding_size + item_dnn_hidden_unit[0]]),
            mindspore.float32),
                                       name='item_bn0_mean')
        self.item_bn0_var = Parameter(Tensor(
            np.ones(
                [item_field_num * embedding_size + item_dnn_hidden_unit[0]]),
            mindspore.float32),
                                      name='item_bn0_var')

        self.item_bn1_gamma = Parameter(Tensor(
            np.ones(
                [item_field_num * embedding_size + item_dnn_hidden_unit[1]]),
            mindspore.float32),
                                        name='item_bn1_gamma')
        self.item_bn1_beta = Parameter(Tensor(
            np.ones(
                [item_field_num * embedding_size + item_dnn_hidden_unit[1]]),
            mindspore.float32),
                                       name='item_bn1_beta')
        self.item_bn1_mean = Parameter(Tensor(
            np.ones(
                [item_field_num * embedding_size + item_dnn_hidden_unit[1]]),
            mindspore.float32),
                                       name='item_bn1_mean')
        self.item_bn1_var = Parameter(Tensor(
            np.ones(
                [item_field_num * embedding_size + item_dnn_hidden_unit[1]]),
            mindspore.float32),
                                      name='item_bn1_var')

        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def construct(self, x1, x2, x3, x4, x5, x6):
        user_field_num, item_field_num, embedding_size = \
            self.user_field_num, self.item_field_num, self.embedding_size
        beta_num = self.beta_num
        # input
        user_input = x1  # None, user_field_num
        item_input = x2  # None, item_field_num
        item_multi_hot_input = x3  # None, 3
        item_multi_hot_input_len = x4  # None, 1
        # select_feature / embedding
        user_reserve_feature = x5  # 3, user_field_num
        user_reserve_embedding = x6  # user_field_num, int(embedding_size / self.beta_num)

        user_xv = ops.gather(self.user_v, user_input, axis=0)
        item_xv = ops.gather(self.item_v, item_input, axis=0)
        item_multi_xv = ops.gather(self.item_v, item_multi_hot_input, axis=0)
        item_multi_xv = item_multi_xv.sum(axis=1)
        item_multi_xv = item_multi_xv / item_multi_hot_input_len
        item_multi_xv = ops.expand_dims(item_multi_xv, axis=1)

        user_xv = ops.reshape(
            user_xv,
            (-1, user_field_num, int(embedding_size / beta_num), beta_num))
        user_embedding_mask1 = ops.expand_dims(user_reserve_embedding, axis=-1)
        user_xv *= user_embedding_mask1
        user_xv = ops.reshape(user_xv, (-1, user_field_num, embedding_size))
        item_xv = ops.concat([item_xv, item_multi_xv], axis=1)

        user_xv0 = ops.reshape(
            user_xv * ops.expand_dims(user_reserve_feature[0], axis=1),
            (-1, user_field_num * embedding_size))
        user_xv1 = ops.reshape(
            user_xv * ops.expand_dims(user_reserve_feature[1], axis=1),
            (-1, user_field_num * embedding_size))
        user_xv2 = ops.reshape(
            user_xv * ops.expand_dims(user_reserve_feature[2], axis=1),
            (-1, user_field_num * embedding_size))
        # user_xv0.set_dtype(mindspore.float32)
        # user_xv1.set_dtype(mindspore.float32)
        # user_xv2.set_dtype(mindspore.float32)
        item_xv0 = ops.reshape(item_xv * 1,
                               (-1, (item_field_num) * embedding_size))
        item_xv1 = ops.reshape(item_xv * 1,
                               (-1, (item_field_num) * embedding_size))
        item_xv2 = ops.reshape(item_xv * 1,
                               (-1, (item_field_num) * embedding_size))
        #
        # user DNN
        h = user_xv0
        h = ops.matmul(h, self.user_w0) + self.user_b0
        h = ops.concat([h, user_xv1], axis=-1)
        h = self.user_bn0(h, self.user_bn0_gamma, self.user_bn0_beta,
                          self.user_bn0_mean, self.user_bn0_var)[0]
        # zcx_test_result = h
        h = self.relu(h)
        h = ops.matmul(h, self.user_w1) + self.user_b1
        h = ops.concat([h, user_xv2], axis=-1)
        h = self.user_bn1(h, self.user_bn1_gamma, self.user_bn1_beta,
                          self.user_bn1_mean, self.user_bn1_var)[0]
        h = self.relu(h)
        user_dnn_out = ops.matmul(h, self.user_w2) + self.user_b2

        # item DNN
        h = item_xv0
        h = ops.matmul(h, self.item_w0) + self.item_b0
        h = ops.concat([h, item_xv1], axis=-1)
        h = self.item_bn0(h, self.item_bn0_gamma, self.item_bn0_beta,
                          self.item_bn0_mean, self.item_bn0_var)[0]
        h = self.relu(h)
        h = ops.matmul(h, self.item_w1) + self.item_b1
        h = ops.concat([h, item_xv2], axis=-1)
        h = self.item_bn1(h, self.item_bn1_gamma, self.item_bn1_beta,
                          self.item_bn1_mean, self.item_bn1_var)[0]
        h = self.relu(h)
        item_dnn_out = ops.matmul(h, self.item_w2) + self.item_b2

        all_score = ops.matmul(user_dnn_out, item_dnn_out.transpose())
        all_ratings = self.sigmoid(all_score)
        return all_ratings
        # return zcx_test_result


alpha = [
    0.39714313, 0.16780508, 0.0005186269, -0.00917678, 0.3416384, -0.02242239,
    0.029514661, 0.44124347, 0.072089575, 0.2662178, 0.4426606, 0.02585494,
    0.11951867, 0.257596, 0.3112677, 0.007339239, 0.3598279, 0.030321935,
    0.27955294, 0.014635027, -0.0006132904, -0.029932514, 0.012801649,
    -0.014725087, -0.02262849, -0.03930172, -0.04083944, 0.046517976,
    0.014259753, -0.012162334, 0.05167715, 0.0040545147, -0.003696015,
    0.011884609, 0.057643216, -0.008003109, 0.022150971, 0.0068186997,
    0.009444783, 0.00548271, 0.009088712, 0.0032386137, 0.007903488,
    0.0028203395, 0.0015112135, 0.0034217522, 0.007061683, -0.00074605,
    -0.0022163545, 0.0054165856, -0.0027527162, -0.0031293896, 0.012429041,
    0.0019685046
]
beta = [
    0.034127895, 0.033376083, 0.035600435, 0.030249925, 0.036311544,
    -0.014257971, 0.047756534, -0.011828341, -0.034527633, 0.015101746,
    0.0005371022, 0.06645153, 0.015395728, 0.020652054, -0.025991265,
    -0.004277289, 0.0039948607, 0.008576264, -0.0121924225, -0.020941442,
    0.014709744, 0.012384741, 0.01867885, 0.016442234, 0.018323518,
    0.007971605, 0.008592229, -0.016125016, 0.0071878238, -0.0018872068,
    -0.0034470966, -0.0067661763, -0.0001335303, 0.015579359, 0.014439113,
    0.016446797, 0.009874817, 0.017658722, 0.020354703, 0.014072323,
    0.016705181, -0.0025958254, 0.01353705, 0.0059331045, -0.0018090425,
    0.012940373, 0.011589683, 0.014106541, 0.00732168, 0.012095058,
    0.011690783, 0.017303353, 0.016023288, 0.019694626, 0.014168393,
    0.022271926, -0.0019669798, -0.0055782897, -0.0017618096, -0.005648979,
    0.0022232202, -0.0027179036, 0.003921953, 0.00897048, 0.014818262,
    0.01579618, 0.006411127, 0.00818153, 0.010983045, 0.0026539934,
    -0.008017432, 0.013552749, 0.015796902, -0.0035538473, 0.003152931,
    0.0106271645, 0.010713519, 0.0060347603, -0.0039533083, 0.010702854,
    0.02181484, 0.026933804, 0.022118941, 0.019825052, 0.020798823,
    -0.0018506732, 0.0036481014, -0.0012732466, -0.0026528533, 0.010365783
]

if __name__ == "__main__":
    tf_model_path = './tf_checkpoints'
    ms_model_path = './ms_checkpoints/tf2mindspore1.ckpt'
    son_structure_performance_logdir = './ms_checkpoints/son_performance.npy'
    son_structure_flop_logdir = './ms_checkpoints/son_structure_flop.npy'
    dataset = Movielens(neg_sample_num=1, importance_way=3)
    user_field_num = dataset.user_field_num
    item_field_num = dataset.item_field_num
    item_multi_field_num = dataset.item_multi_field_num

    user_feature_num = dataset.user_feature_num
    item_feature_num = dataset.item_feature_num
    network = DSSM_multi_input_once_for_all(
        user_field_num=user_field_num,
        item_field_num=item_field_num,
        user_feature_num=user_feature_num,
        item_feature_num=item_feature_num,
        embedding_size=40,
        user_dnn_hidden_unit=[512, 256, 128],
        item_dnn_hidden_unit=[512, 256, 128])

    params_dict = load_checkpoint(ms_model_path)
    load_param_into_net(network, params_dict)

    son_structure_performance_dict = np.load(son_structure_performance_logdir,
                                             allow_pickle=True).item()
    son_structure_flop_dict = np.load(son_structure_flop_logdir,
                                      allow_pickle=True).item()
    all_son_structure_performance_dict = []
    for key in son_structure_performance_dict:
        tmp_tuple = (key, son_structure_performance_dict[key][0],
                     son_structure_flop_dict[key])
        all_son_structure_performance_dict.append(tmp_tuple)
    all_son_structure_performance_sorted_list = sorted(
        all_son_structure_performance_dict,
        key=lambda x: (-x[1], x[2], x[0][0], x[0][1]))
    user_reserve_feature_list, user_reserve_embedding_list = [], []
    son_structure_flop_list = []

    dssm_flop_num = 705164
    min_flop_num = 0.75 * dssm_flop_num
    max_flop_num = 1.25 * dssm_flop_num
    strict_flop_list = []
    for strict_i in range(9, -1, -1):
        tmp_flop_num = min_flop_num + int(
            (max_flop_num - min_flop_num) * strict_i / 9)
        strict_flop_list.append(tmp_flop_num)
    print(max_flop_num)
    print(min_flop_num)
    print(strict_flop_list)
    print(len(strict_flop_list))
    argsort_alpha = np.argsort(-np.abs(alpha))  # 3, 18
    argsort_beta = np.argsort(-np.abs(beta))  # 18, 5
    avg_flop_num = 0

    for strict_i in range(10):
        tmp_structure_top1 = None
        for structure_i in range(
                len(all_son_structure_performance_sorted_list)):
            tmp_structure, tmp_performance, tmp_flop = all_son_structure_performance_sorted_list[
                structure_i]
            if son_structure_flop_dict[tmp_structure] <= strict_flop_list[
                    strict_i]:
                # tmp_tuple = (
                # tmp_structure[0], sum(tmp_structure[1:]), son_structure_performance_dict[tmp_structure],
                # son_structure_flop_dict[tmp_structure])
                tmp_tuple = (tmp_structure[0], sum(tmp_structure[1:]),
                             son_structure_performance_dict[tmp_structure],
                             son_structure_flop_dict[tmp_structure])
                tmp_structure_top1 = tmp_tuple
                break
        if tmp_structure_top1 is None:
            print("wrong")
            exit(-1)
        print("son structure:", tmp_structure_top1)
        son_structure_flop_list.append(tmp_structure_top1[-1])
        avg_flop_num += tmp_structure_top1[-1]
        tmp_user_select_feature_num = tmp_structure_top1[0]
        tmp_user_select_embedding_num = tmp_structure_top1[1]
        tmp_user_select_feature_matrix = np.zeros(shape=(54))
        for j in range(tmp_user_select_feature_num):
            tmp_user_select_feature_matrix[argsort_alpha[j]] = 1
        tmp_user_select_feature_matrix = np.reshape(
            tmp_user_select_feature_matrix, (3, 18))

        tmp_user_select_embedding_matrix = np.zeros(shape=(18, 5))
        user_embedding_len = np.zeros(shape=(18), dtype=np.int32)
        remain_num = tmp_user_select_embedding_num
        for i in range(remain_num):
            tmp_feature = argsort_beta[i]
            user_embedding_len[int(tmp_feature / 5)] += 1
        for i in range(18):
            tmp_user_select_embedding_matrix[i, 0:user_embedding_len[i]] = 1
        user_reserve_feature_list.append(tmp_user_select_feature_matrix)
        user_reserve_embedding_list.append(tmp_user_select_embedding_matrix)
    for strict_i in range(10):
        print(son_structure_flop_list[strict_i])

    all_test_item_one_hot, all_test_item_multi_hot = dataset.get_all_item()
    all_test_item_one_hot = all_test_item_one_hot  # [:3]
    all_test_item_multi_hot = all_test_item_multi_hot  # [:3]
    Ks = [10, 20]  # matrix@k
    result = {'recall': np.zeros(len(Ks)), 'ndcg': np.zeros(len(Ks))}
    count = 0
    n_test_users = len(dataset.only_test_user_order)

    for split_user in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
        test_data_param = {
            'part': 'new_test_split',  # new_test_way1_split
            'shuffle': False,
            'batch_size': 1024,
            'split_user': split_user,
        }
        split_user_num = len(dataset.test_split_order[split_user])
        split_result = [0.0, 0.0]
        tmp_count = 0

        for batch_data in dataset.batch_generator(test_data_param):
            user_feature_batch, user_click_item_train_batch, user_click_item_test_batch = batch_data
            uid_batch = user_feature_batch[:, 0]
            current_batch_size = uid_batch.shape[0]
            np_in1 = Tensor(user_feature_batch)
            np_in2 = Tensor(all_test_item_one_hot)
            np_in3 = Tensor(all_test_item_multi_hot[:, :-1])
            np_in4 = Tensor(all_test_item_multi_hot[:, -1:])
            np_in5 = Tensor(
                np.array(user_reserve_feature_list[split_user],
                         dtype=np.float32))
            np_in6 = Tensor(
                np.array(user_reserve_embedding_list[split_user],
                         dtype=np.float32))
            print(np_in1.shape, np_in2.shape, np_in3.shape, np_in4.shape,
                  np_in5.shape, np_in6.shape)
            outs = network(np_in1, np_in2, np_in3, np_in4, np_in5, np_in6)
            print(outs.shape)
            ratings_batch = outs
            uid_batch = user_feature_batch[:, 0]
            print("begin")
            user_batch_rating_uid = zip(ratings_batch, uid_batch,
                                        user_click_item_train_batch,
                                        user_click_item_test_batch)
            batch_result = []
            tmp_num = 0
            for x1, x2, x3, x4 in user_batch_rating_uid:
                tmp_num += 1
                batch_result.append(test_one_user((x1, x2, x3, x4), Ks))
                if tmp_num % 10 == 0:
                    print("tmp_num:", tmp_num)

            print("end")
            for re in batch_result:
                result['recall'] += re['recall'] / n_test_users
                result['ndcg'] += re['ndcg'] / n_test_users
            for re in batch_result:
                split_result[0] += re['recall'][1] / split_user_num
                split_result[1] += re['ndcg'][1] / split_user_num
            count += current_batch_size
            tmp_count += current_batch_size
            print("count1:", tmp_count, "sum1:", split_user_num, "count2:",
                  count, "sum2:", n_test_users)
        split_result = np.round(split_result, 4)
        print("part user:", split_user, "split_result:", split_result,
              "split_user_num:", split_user_num)

    print("count:", count, "n_test_users:", split_user_num)
    print("result:", result)
