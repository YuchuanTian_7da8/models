# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
import mindspore as ms
from mindspore import nn
from mindspore import ops as P
from mindspore.common.initializer import initializer, TruncatedNormal
ms.set_context(mode=ms.PYNATIVE_MODE)


class SelfAttention(nn.Cell):
    """
    cross-attention/self-attention/distance-aware attention
    in_dim(int): dim of input
    keep_prob(float): keep prob
    num_unit(int): number of unit in self-attention, default: None, use the dim of input vector
    num_head(int): number of head, default 1
    dist_aware(bool): use distant-aware attention, default False
    """
    def __init__(self, in_dim, keep_prob, num_units=None, num_head=1, dist_aware=False):
        super(SelfAttention, self).__init__()
        self.num_units = num_units
        self.num_head = num_head
        self.dist_aware = dist_aware

        if num_units is None:
            num_units = in_dim
        self.key_dense = nn.Dense(in_dim, num_units)
        self.query_dense = nn.Dense(in_dim, num_units)
        self.value_dense = nn.Dense(in_dim, num_units)

        if dist_aware:
            self.w = ms.Tensor(-0.1, dtype=ms.float32)
            self.v = ms.Parameter(default_input=initializer('zeros', [num_head, 1, 1], ms.float32), name='v')

        self.concat0 = P.Concat(axis=0)
        self.concat2 = P.Concat(axis=2)
        self.split0 = P.Split(axis=0, output_num=num_head)
        self.split2 = P.Split(axis=2, output_num=num_head)
        self.transpose = P.Transpose()
        self.tile = P.Tile()
        self.expand_dims = P.ExpandDims()
        self.reshape = P.Reshape()
        self.softplus = P.Softplus()
        self.exp = P.Exp()
        self.shape = P.Shape()
        self.sign = P.Sign()
        self.abs = P.Abs()
        self.ones_likes = P.OnesLike()
        self.matmul = P.BatchMatMul()
        self.softmax = P.Softmax()
        self.dropout = P.Dropout(keep_prob=keep_prob)
        self.equal = P.Equal()
        self.reduce_sum = P.ReduceSum()

    def construct(self, queries, keys, values, dist_mat=None):
        """
        :param queries: [bs, seq_len, query_ft_num]
        :param keys: [bs, seq_len, key_ft_num]
        :param values: [bs, seq_len, value_ft_num]
        :param dist_mat: [seq_len, seq_len], used in distance-aware attention
        :return: [bs, seq_len, value_ft_num]
        """
        q = self.query_dense(queries)
        k = self.key_dense(keys)
        v = self.value_dense(values)

        q_ = self.concat0(self.split2(q))
        k_ = self.concat0(self.split2(k))
        v_ = self.concat0(self.split2(v))

        outputs = self.matmul(q_, self.transpose(k_, (0, 2, 1)))

        if self.dist_aware and dist_mat is not None:
            seq_len = self.shape(queries)[-2]
            batch_size = self.shape(queries)[0]

            r = self.tile(self.expand_dims(dist_mat, 0), (self.num_head, 1, 1))
            r_ = (1 + self.exp(self.v)) / (1 + self.exp(self.v - self.w * r))
            r_ = self.tile(self.expand_dims(r_, 1), (1, batch_size, 1, 1))
            r_ = self.reshape(r_, (-1, seq_len, seq_len))
            outputs = self.softplus(outputs) * r_

        outputs = outputs / (self.shape(keys)[-1] ** 0.5)

        key_masks = self.sign(self.abs(self.reduce_sum(keys, axis=-1)))
        key_masks = self.tile(key_masks, (self.num_head, 1))
        key_masks = self.tile(self.expand_dims(key_masks, 1), (1, self.shape(queries)[1], 1))

        paddings = self.ones_likes(outputs)
        outputs = ms.numpy.where(key_masks == 0, paddings, outputs)
        outputs = self.softmax(outputs)

        query_masks = self.sign(self.abs(self.reduce_sum(queries, axis=-1)))
        query_masks = self.tile(query_masks, (self.num_head, 1))
        query_masks = self.tile(self.expand_dims(query_masks, 1), (1, self.shape(queries)[1], 1))

        outputs *= query_masks
        outputs, _ = self.dropout(outputs)
        outputs = self.matmul(outputs, v_)
        outputs = self.concat2(self.split0(outputs))
        return outputs


class EncoderLayer(nn.Cell):
    '''
    Encoder of transformer
    '''
    def __init__(self, inp_dim, keep_prob, d_model, d_inner, num_head=1):
        super(EncoderLayer, self).__init__()
        self.self_attention = SelfAttention(inp_dim, keep_prob, d_model, num_head, dist_aware=False)
        self.bn1 = nn.BatchNorm1d(d_model)
        self.fc1 = nn.Dense(d_model, d_inner, activation=nn.ReLU())
        self.dp = nn.Dropout(keep_prob)
        self.fc2 = nn.Dense(d_inner, d_model, activation=None)
        self.bn2 = nn.BatchNorm1d(d_model)
        self.reshape = P.Reshape()

    def construct(self, inp):
        '''
        :param inp: [bs, seq_len, inp_dim]
        :return: [bs, seq_len, inp_dim]
        '''
        att_out = self.self_attention(inp, inp, inp)
        att_shape = att_out.shape
        bn_inp = self.reshape(att_out, (-1, att_shape[-1]))
        bn1 = self.bn1(bn_inp)
        bn1 = self.reshape(bn1, att_shape)
        fc1 = self.fc1(bn1)
        dp = self.dp(fc1)
        fc2 = self.fc2(dp)
        fc2 = fc2 + att_out
        fc2_shape = fc2.shape
        fc2 = self.reshape(fc2, (-1, fc2_shape[-1]))
        output = self.bn2(fc2)
        output = self.reshape(output, fc2_shape)
        return output


class DSAttn(nn.Cell):
    '''
    Dual-side attention
    '''
    def __init__(self, v_seq_len, v_dim, q_seq_len, q_dim):
        super(DSAttn, self).__init__()
        self.v_seq_len = v_seq_len
        self.v_dim = v_dim
        self.q_seq_len = q_seq_len
        self.q_dim = q_dim
        self.w_b = ms.Parameter(default_input=initializer(TruncatedNormal(sigma=1.0),
                                                          [1, q_dim, v_dim], ms.float32), name='w_b')
        self.w_v = ms.Parameter(default_input=initializer(TruncatedNormal(sigma=1.0),
                                                          [1, v_dim, v_seq_len], ms.float32), name='w_v')
        self.w_q = ms.Parameter(default_input=initializer(TruncatedNormal(sigma=1.0),
                                                          [1, q_dim, v_seq_len], ms.float32), name='w_q')
        self.bat_mat_mul = P.BatchMatMul()
        self.mat_mul = P.MatMul()
        self.tanh = P.Tanh()
        self.reshape = P.Reshape()
        self.transpose = P.Transpose()
        self.softmax = P.Softmax(axis=-1)
        self.shape = P.Shape()
        self.tile = P.Tile()

    def construct(self, v, q):
        '''
        :param v: values, [bs, v_seq_len, v_dim]
        :param q: queries, [bs, q_seq_len, q_dim]
        :return: attentioned v and q with shape of [bs, v_seq_len, v_dim] and [bs, v_seq_len, q_dim]
        '''
        batch_size = v.shape[0]
        v_t = self.transpose(v, (0, 2, 1))
        w_b = self.tile(self.w_b, (batch_size, 1, 1))
        c = self.bat_mat_mul(q, self.bat_mat_mul(w_b, v_t))
        c_t = self.transpose(c, (0, 2, 1))

        w_v = self.tile(self.w_v, (batch_size, 1, 1))
        w_q = self.tile(self.w_q, (batch_size, 1, 1))
        hv_1 = self.bat_mat_mul(v, w_v)
        hq_1 = self.bat_mat_mul(q, w_q)
        hq_1 = self.transpose(hq_1, (0, 2, 1))
        h_v = self.tanh(hv_1 + self.bat_mat_mul(hq_1, c))
        h_q = self.tanh(hq_1 + self.bat_mat_mul(hv_1, c_t))

        a_v = self.softmax(h_v)
        a_q = self.softmax(h_q)
        v = self.bat_mat_mul(a_v, v)
        q = self.bat_mat_mul(a_q, q)
        return v, q


class Aggregation(nn.Cell):
    def __init__(self, inp_dim):
        super(Aggregation, self).__init__()
        self.context = ms.Parameter(default_input=initializer(TruncatedNormal(sigma=1.0),
                                                              [1, 1, inp_dim], ms.float32), name='context')
        self.dense = nn.Dense(inp_dim, inp_dim, activation=nn.Tanh())
        self.softmax = P.Softmax(axis=1)
        self.reduce_sum_keep = P.ReduceSum(keep_dims=True)
        self.reduce_sum = P.ReduceSum(keep_dims=False)
        self.reshape = P.Reshape()

    def construct(self, inp):
        '''
        :param inp: [bs, seq_len, inp_dim]
        :return: [bs, inp_dim]
        '''
        h = self.dense(inp)
        alpha = self.reduce_sum_keep(ms.numpy.multiply(h, self.context), axis=2)
        alpha = self.softmax(alpha)
        output = self.reduce_sum(ms.numpy.multiply(inp, alpha), axis=1)
        return output


class MLP(nn.Cell):
    def __init__(self, inp_dim, kp, layers):
        super(MLP, self).__init__()
        self.batch_norm = nn.BatchNorm1d(inp_dim)
        self.reshape = P.Reshape()
        self.layers = [inp_dim] + layers  # [224, 256, 64]
        self.layer_num = len(layers)   # 2
        seq = [[nn.Dense(self.layers[i], self.layers[i + 1]), nn.ReLU(), nn.Dropout(kp)]
               for i in range(self.layer_num)]
        self.seq = nn.SequentialCell(sum(seq, []))

    def construct(self, inp):
        inp_shape = inp.shape
        inp = self.reshape(inp, (-1, inp_shape[-1]))
        bn = self.batch_norm(inp)
        fc = self.seq(bn)
        fc = self.reshape(fc, inp_shape[:-1] + (self.layers[-1],))
        return fc


class PAR(nn.Cell):
    """
    Page-level Attentional Reranking from paper: "A Bird's-eye View of Reranking:
    from List level to Page Level"
    """
    def __init__(self, args):
        super(PAR, self).__init__()
        self.feat_size = args.feat_size
        self.list_num = args.list_num
        self.list_len = args.list_len
        self.itm_spar_fnum = args.itm_spar_fnum
        self.itm_dens_fnum = args.itm_dens_fnum
        self.usr_fnum = args.usr_fnum
        self.hist_fnum = args.hist_fnum
        self.emb_dim = args.emb_dim
        self.max_hist_len = args.max_hist_len
        self.hidden_size = args.d_model
        self.max_grad_norm = args.grad_norm
        self.l2_norm = args.l2_norm
        self.lr = args.lr
        self.kp = args.keep_prob
        self.expert_num = args.expert_num
        self.itm_ft_dim = self.itm_spar_fnum * self.emb_dim + self.itm_dens_fnum
        self.hist_ft_dim = self.hist_fnum * self.emb_dim
        self.usr_ft_dim = self.usr_fnum * self.emb_dim
        self.max_len_list = [self.list_len] * self.list_num

        self.emb_mtx = nn.Embedding(self.feat_size + 1, self.emb_dim, embedding_table=TruncatedNormal(sigma=1.0))

        self.reshape = P.Reshape()
        self.shape = P.Shape()
        self.tile = P.Tile()
        self.unstack = P.Unstack(axis=1)
        self.concat = P.Concat(axis=-1)
        self.concat1 = P.Concat(axis=1)
        self.stack = P.Stack(axis=1)
        self.stack2 = P.Stack(axis=2)
        self.expand_dims = P.ExpandDims()
        self.softmax = P.Softmax(axis=-1)
        self.squeeze2 = P.Squeeze(axis=2)
        self.bat_mat_mul = P.BatchMatMul()
        self.split = P.Split(axis=1, output_num=self.list_num)

        self.dist_mat = self.get_distant_matrix()
        self.spatial_scale_attn = SelfAttention(self.itm_ft_dim, self.kp, args.d_model, args.n_head, dist_aware=True)
        self.dual_side_attn = DSAttn(self.list_len, self.itm_ft_dim, self.max_hist_len, self.hist_ft_dim)
        self.list_aggregation = Aggregation(args.d_model)
        self.item_aggregation = Aggregation(self.hist_ft_dim + self.itm_ft_dim)
        self.item_attn = EncoderLayer(self.itm_ft_dim + self.hist_ft_dim, self.kp, args.d_model,
                                      args.d_inner, args.n_head)

        self.deep_net = MLP(self.usr_ft_dim + self.itm_ft_dim, self.kp, [256, 64])
        mmoe_inp_dim = self.itm_ft_dim * 2 + self.hist_ft_dim + args.d_model * 2 + 64
        self.gate_nets = nn.CellList([MLP(mmoe_inp_dim, self.kp, [args.expert_num]) for _ in range(self.list_num)])
        self.expert_nets = nn.CellList([MLP(mmoe_inp_dim, self.kp, [200, 80]) for _ in range(args.expert_num)])
        self.tower_nets = nn.CellList([MLP(80, self.kp, [80]) for _ in range(self.list_num)])
        self.final_fc = nn.CellList([nn.Dense(80, 1, activation=nn.ReLU()) for _ in range(self.list_num)])

    def construct(self, user_ft, spar_ft, dens_ft, user_hist, length):
        """
        Args:
            usr_ft: batch user sparse feature(id); [bs, user_fnum]
            spar_ft: batch item sparse feature(id);   [bs, list_num, list_len, itm_spar_fnum]
            dens_ft: batch item dense feature;   [bs, list_num, list_len, itm_dens_fnum]
            user_hist: batch user behavior history(item sparse feature); [bs, max_hist_len, hist_fnum]
            length: batch length of lists; [bs, list_num]
        """
        # embedding
        spar_emb = self.emb_mtx(spar_ft)
        spar_emb = self.reshape(spar_emb, (-1, self.list_num, self.list_len, self.emb_dim * self.itm_spar_fnum))

        emb = self.concat([spar_emb, dens_ft])
        page = self.unstack(emb)

        user_emb = self.emb_mtx(user_ft)
        user_emb = self.reshape(user_emb, (-1, 1, 1, self.usr_fnum * self.emb_dim))
        tile_user = self.tile(user_emb, (1, self.list_num, self.list_len, 1))
        tile_user = self.reshape(tile_user, (-1, self.list_num, self.list_len, self.usr_fnum * self.emb_dim))
        page_user = self.unstack(tile_user)

        hist_emb = self.emb_mtx(user_hist)
        hist_emb = self.reshape(hist_emb, (-1, self.max_hist_len, self.hist_fnum * self.emb_dim))
        len_list = self.unstack(length)

        # spatial scale attention
        cmp_inp = self.concat1(page)
        cmp_res = self.spatial_scale_attn(cmp_inp, cmp_inp, cmp_inp, self.dist_mat)
        cmp_list = self.split(cmp_res)

        # hierarchical dual-side attention
        channel_list, item_att_list = [], []
        for i, list_emb in enumerate(page):
            item_att, hist_att = self.dual_side_attn(list_emb, hist_emb)
            att_vec = self.concat([item_att, hist_att])
            item_att_list.append(att_vec)
            channel_list.append(self.item_aggregation(att_vec))

        channels = self.stack(channel_list)
        channel_att = self.item_attn(channels)
        page_vec = self.list_aggregation(channel_att)

        # mmoe
        y_preds = []
        for i, list_emb in enumerate(page):
            user_list = self.concat([list_emb, page_user[i]])
            deep_res = self.deep_net(user_list)

            tile_page_vec = self.tile(self.expand_dims(page_vec, 1), (1, self.max_len_list[i], 1))
            inp = self.concat([list_emb, deep_res, tile_page_vec, item_att_list[i], cmp_list[i]])
            tower_rep = self.mmoe(inp, inp, i)
            y_pred = self.final_fc[i](tower_rep)
            y_pred = self.squeeze2(y_pred)
            mask = self.sequence_mask(len_list[i], self.max_len_list[i])
            y_preds.append(y_pred * mask)
        y_preds = self.stack(y_preds)
        return y_preds

    def sequence_mask(self, inp, max_len):
        batch_num = self.shape(inp)[0]
        rang = self.tile(self.reshape(P.arange(max_len), (1, max_len)), (batch_num, 1))
        mask = rang < self.expand_dims(inp, -1)
        return mask.astype(ms.float32)

    def mmoe(self, inp, gate_inp, task):
        gate = self.gate_nets[task](gate_inp)
        gate = self.expand_dims(self.softmax(gate), -2)
        expert_res = []
        for i in range(self.expert_num):
            expert_res.append(self.expert_nets[i](inp))
        expert_res = self.stack2(expert_res)
        fin_inp = self.squeeze2(self.bat_mat_mul(gate, expert_res))
        output = self.tower_nets[task](fin_inp)
        return output

    def get_distant_matrix(self, mode='manhattan'):
        if mode == 'manhattan':
            itm_pos = {}
            for i in range(self.list_num):
                for j in range(self.list_len):
                    itm_pos[i * self.list_len + j] = (i, j)
            itm_num = len(itm_pos)
            dist_mat = np.zeros((itm_num, itm_num))
            for i in range(itm_num):
                for j in range(itm_num):
                    dist_mat[i][j] = abs(itm_pos.get(i)[0] - itm_pos.get(j)[0]) + \
                                     abs(itm_pos.get(i)[1] - itm_pos.get(j)[1])
            print('item 5-6', dist_mat[5][6], 'item 5-7', dist_mat[5][7],
                  'item 5-24', dist_mat[5][24], 'item 5-25', dist_mat[5][25])
            print('item 24-25', dist_mat[24][25], 'item 25-6', dist_mat[25][6], 'item 25-7', dist_mat[25][7])
            return ms.Tensor(dist_mat, dtype=ms.float32)

        return None


class PARWithLossCell(nn.Cell):
    def __init__(self, backbone, loss_fn):
        super(PARWithLossCell, self).__init__(auto_prefix=False)
        self._backbone = backbone
        self._loss_fn = loss_fn

    def construct(self, usr_ft, spar_ft, dens_ft, hist_ft, length, lb, clk):
        out = self._backbone(usr_ft, spar_ft, dens_ft, hist_ft, length)
        _ = lb
        return self._loss_fn(out, clk)
