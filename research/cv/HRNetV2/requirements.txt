mindspore~=2.1.0
numpy~=1.21.2
opencv-python~=4.5.4.58
pycocotools>=2.0.5
matplotlib
seaborn
tqdm==4.64.1
decorator~=5.1.1
