# Copyright 2021-2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================


import torch
import torch.nn as nn
import torchvision.models as models


def useless():
    checkpoint = torch.load('../rpg_models/98.pth', map_location='cpu')['state_dict']
    model = nn.DataParallel(models.resnet50())
    model.load_state_dict(checkpoint)
    # model = model.module
    # dummy_inputs = (torch.randn(3,224,224),)



    for k,v in checkpoint.items():
        print(k)
    
def transformation(s):
    # s: torch key
    if 'num_batches_tracked' in s:
        return None
    # structure
    s = s.replace('module.','')
    # bn
    if ('bn' in s) or ('downsample.1' in s):
        s = s.replace('running_var', 'moving_variance')
        s = s.replace('running_mean', 'moving_mean')
        s = s.replace('weight', 'gamma')
        s = s.replace('bias', 'beta')
    

    # structure
    s = s.replace('downsample', 'down_sample_layer')
    s = s.replace('fc', 'end_point')
    return s