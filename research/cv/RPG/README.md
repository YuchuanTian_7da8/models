# Towards Higher Ranks via Adversarial Weight Pruning (RPG)

*Yuchuan Tian, Hanting Chen, Tianyu Guo, Chao Xu, Yunhe Wang*

[[Arxiv]](https://arxiv.org/abs/2311.17493) [[PyTorch Code]](https://github.com/huawei-noah/Efficient-Computing/tree/master/Pruning/RPG)

The official MindSpore code of our NeurIPS 2023 paper "Towards Higher Ranks via Adversarial Weight Pruning".

*BibTex* formatted citation:

```latex
@misc{tian2023higher,
      title={Towards Higher Ranks via Adversarial Weight Pruning}, 
      author={Yuchuan Tian and Hanting Chen and Tianyu Guo and Chao Xu and Yunhe Wang},
      year={2023},
      eprint={2311.17493},
      archivePrefix={arXiv},
      primaryClass={cs.CV}
}
```

In the paper, the **RPG** unstructured pruning method is proposed. RPG guides sparse weights towards higher matrix ranks via the proposed adversarial rank loss. The diagram of RPG is shown as follows:

<img src="img/RPG_diagram.PNG" alt="RPG_diagram" style="zoom:50%;" />

## Inference

Here we provide the procedures for running sparse ResNet-50 models on the ImageNet 2012 validation set:

**Preparation**

Install supporting packages: ```pip install -r requirements.txt ```

Download ImageNet2012 Validation Set

Download pruned ResNet-50 weights (see #pruned weights)

**Configure**

Adjust parsers in ```config_prune/resnet50_imagenet2012_config.yaml```:

```device_target```: [CPU, GPU, Ascend];

```checkpoint_file_path```: path to the sparse weight to be evaluated;

```datapath```: path to the ImageNet2012 val dataset.

**Running**

Run the following command to perform inference:

```shell
python infer_prune.py 
```

## Pruned Weights

Here we provide Google Drive and Baidu Netdisk links to pruned ResNet-50 weights. The weights are in PyTorch format, but they are automatically transformed to MindSpore format in the ```infer_prune.py ``` script.

**ResNet-50 on ImageNet Experiments**

| Sparsity | ImageNet Top1 Acc. | Google Drive                                                 | Baidu Netdisk                                                |
| -------- | ------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 80       | 76.66              | [Link](https://drive.google.com/file/d/1-x3f_PIcSZkmhv7-X9vBpXN-zSp7W9F3/view?usp=drive_link) | [Link (PIN:1234)](https://pan.baidu.com/s/1AUqzU4uA7RW9gQRCssXOeg) |
| 90       | 75.80              | [Link](https://drive.google.com/file/d/10-nz5vYoE-qXp0nhAJ_9CYeHLJEr2CXI/view?usp=drive_link) | [Link (PIN:1234)](https://pan.baidu.com/s/11rwVrwtc-mnL87tnShbaBw) |
| 95       | 74.05              | [Link](https://drive.google.com/file/d/107NmBo_DP_Niit6QxAF0qhdkcnXi6hDC/view?usp=drive_link) | [Link (PIN:1234)](https://pan.baidu.com/s/1w-ykBeGa1ZNw04rzbLWC-A) |
| 98       | 69.57              | [Link](https://drive.google.com/file/d/1-mXDm0qyCANxD2-Y-oqf-Obi-omWq1UT/view?usp=drive_link) | [Link (PIN:1234)](https://pan.baidu.com/s/1XGG75o5tineDiJ6YIFkvow) |

**ResNet-50 Pruned from Scratch**

| Sparsity | ImageNet Top1 Acc. | Google Drive                                                 | Baidu Netdisk                                                |
| -------- | ------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 90       | 75.35              | [Link](https://drive.google.com/file/d/103wYuFpmJj3Bo3InXj-iKmgKULyY3ECH/view?usp=drive_link) | [Link (PIN:1234)](https://pan.baidu.com/s/1ZpF_Cf7jupBhnwR8u7uEZg) |
| 95       | 73.62              | [Link](https://drive.google.com/file/d/1-lUETG6EZu_GpQMz1OriKxHf2OOeKJ3_/view?usp=drive_link) | [Link (PIN:1234)](https://pan.baidu.com/s/1jV0nYHhHEqcUZMa2P0tShA) |

## Acknowledgement

Our code refers to the official MindSpore repo: 

https://gitee.com/mindspore/models.git

We thank the authors for open-sourcing.

## License

We adopt license Apache-2.0.

