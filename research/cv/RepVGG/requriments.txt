numpy==1.21.6
onnxruntime-gpu==1.13.1
PyYAML==6.0
matplotlib==3.5.3
Pillow==9.2.0
tqdm==4.64.1
