# Contents

* [Contents](#contents)
    * [Foveabox Description](#foveabox-description)
        * [Model Architecture](#model-architecture)
        * [Dataset](#dataset)
    * [Environment Requirements](#environment-requirements)
    * [Quick Start](#quick-start)
        * [Prepare the model](#prepare-the-model)
        * [Run the scripts](#run-the-scripts)
    * [Script Description](#script-description)
        * [Script and Sample Code](#script-and-sample-code)
        * [Script Parameters](#script-parameters)
    * [Training](#training)
        * [Training process](#training-process)
    * [Evaluation](#evaluation)
        * [Evaluation process](#evaluation-process)
            * [Evaluation on GPU](#evaluation-on-gpu)
        * [Evaluation result](#evaluation-result)
    * [Model Description](#model-description)
        * [Performance](#performance)
    * [Description of Random Situation](#description-of-random-situation)
    * [ModelZoo Homepage](#modelzoo-homepage)

## [Foveabox Description](#contents)

FoveaBox is an accurate, flexible, and completely anchor-free framework for
object detection. While almost all state-of-the-art object detectors utilize
predefined anchors to enumerate possible locations, scales and aspect ratios
for the search of the objects, their performance and generalization ability
are also limited to the design of anchors. Instead, FoveaBox directly learns
the object existing possibility and the bounding box coordinates without
anchor reference. This is achieved by: (a) predicting category-sensitive
semantic maps for the object existing possibility, and (b) producing
category-agnostic bounding box for each position that potentially contains
an object. The scales of target boxes are naturally associated with feature
pyramid representations. In FoveaBox, an instance is assigned to adjacent
feature levels to make the model more accurate.Authors demonstrate its
effectiveness on standard benchmarks and report extensive experimental
analysis. Without bells and whistles, FoveaBox achieves state-of-the-art
single model performance on the standard COCO and Pascal VOC object detection
benchmark. More importantly, FoveaBox avoids all computation and
hyper-parameters related to anchor boxes, which are often sensitive to
the final detection performance. Authors believe the simple and effective
approach will serve as a solid baseline and help ease future research for
object detection.

[Paper](https://arxiv.org/pdf/1904.03797.pdf): Tao Kong, Fuchun Sun, Fellow,
Huaping Liu, Yuning Jiang, Lei Li, and Jianbo Shi. FoveaBox: Beyond Anchor-Based Object Detection (2019).

### [Model Architecture](#contents)

Foveabox contains a backbone network and a fovea head network.
The backbone is responsible for computing a convolutional feature map over an entire
input image and is an off-the-shelf convolutional network. The fovea head is
composed of two sub-branches, the first branch performs per pixel classification
on the backbone’s output; the second branch performs box prediction for each position that
potentially covered by an object.

### [Dataset](#contents)

Note that you can run the scripts based on the dataset mentioned in original
paper or widely used in relevant domain/network architecture. In the following
sections, we will introduce how to run the scripts using the related dataset
below.

Dataset used: [COCO-2017](https://cocodataset.org/#download)

* Dataset size: 25.4G
    * Train: 18.0G，118287 images
    * Val: 777.1M，5000 images
    * Test: 6.2G，40670 images
    * Annotations: 474.7M, represented in 3 JSON files for each subset.
* Data format: image and json files.
    * Note: Data will be processed in dataset.py

## [Environment Requirements](#contents)

* Install [MindSpore](https://www.mindspore.cn/install/en).
* Download the dataset COCO-2017.
* Install third-parties requirements:

```text
numpy~=1.21.6
opencv-python~=4.8.1
pycocotools~=2.0.7
PyYAML~=6.0
seaborn~=0.12.2
tqdm~=4.64.1
```

* We use COCO-2017 as training dataset in this example by default, and you
 can also use your own datasets. Dataset structure:

```shell
.
└── coco-2017
    ├── train
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    ├── validation
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    └── test
        ├── data
        │    ├── 000000000001.jpg
        │    ├── 000000000002.jpg
        │    └── ...
        └── labels.json
```

* Also we make evaluation only on mindrecord converted dataset. Use the
 `convert_dataset.py` script to convert original COCO subset to mindrecord.

```shell
python convert_dataset.py --config_path default_config.yaml --converted_coco_path data/coco-2017/validation --converted_mindrecord_path data/mindrecord/validation
```

Result mindrecord dataset will have next format:

```shell
.
└── mindrecord
    ├── validation
    │   ├── file.mindrecord
    │   ├── file.mindrecord.db
    │   └── labels.json
    └── test
        ├── file.mindrecord
        ├── file.mindrecord.db
        └── labels.json
```

It is possible to convert train dataset to mindrecord format and use it to
train model.

## [Quick Start](#contents)

### [Prepare the model](#contents)

1. Prepare yaml config file. Create file and copy content from any config file, e.g.
 `fovea_r50_fpn_4x4_1x_coco.yaml` to created file.
2. Change data settings: experiment folder (`train_outputs`), image size
 settings (`img_width`, `img_height`, etc.), subsets folders (`train_dataset`,
 `val_dataset`, `train_data_type`, `val_data_type`), information about
 categories etc.
3. Change the backbone settings: type (`backbone.type`), path to pretrained
 ImageNet weights (`backbone.pretrained`), layer freezing settings
 (`backbone.frozen_stages`).
4. Change other training hyper parameters (learning rate, regularization,
 augmentations etc.).
5. Prepare pre-trained checkpoints.

### [Run the scripts](#contents)

After installing MindSpore via the official website, you can start training and
evaluation as follows:

* running on GPU

```shell
# standalone training on GPU
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]

# run eval on GPU
bash scripts/run_distribute_train_gpu.sh [CONFIG_PATH] [DEVICE_NUM] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]
```

## [Script Description](#contents)

### [Script and Sample Code](#contents)

```commandline
foveabox
├── configs  # Configurations
│   ├── fovea_align_r101_fpn_gn-head_mstrain_640-800_4x4_2x_coco.yaml
│   └── fovea_align_r50_fpn_gn-head_mstrain_640-800_4x4_2x_coco.yaml
├── scripts
│   ├── run_distribute_train_gpu.sh  # Script to run distributed training on multiple GPUs
│   ├── run_eval_gpu.sh  # Script to run evaluation
│   ├── run_infer_gpu.sh  # Script to run inference
│   └── run_standalone_train_gpu.sh  # Script to run training on single GPU
├── src
│   ├── callback.py  # Callbacks used in training
│   ├── common.py  # Some common utils
│   ├── config.py  # Script to parse config yaml and merge with CLI
│   ├── dataset.py  # Data loading and preprocessing
│   ├── detecteval.py  # Utils for detection evaluation
│   ├── eval_utils.py  # Utils for detection evaluation
│   ├── foveabox
│   │   ├── base_dense_head.py  # Common dense head
│   │   ├── bbox_head.py  # Foveabox detection head
│   │   ├── conv_module.py  # Common class for creating conv layers
│   │   ├── deform_conv2d.py  # Deformable convolution implementation
│   │   ├── foveabox.py  # Foveabox detector implementation
│   │   ├── fpn.py  # FPN
│   │   ├── initialization.py  # Functions for layer initialization
│   │   ├── __init__.py
│   │   ├── loss.py  # FocalLoss implementation
│   │   ├── point_generator.py  # Point generator for point-based detector
│   │   ├── resnet.py  # Backbone implementation
│   │   └── utils.py  # Aux utils
│   ├── __init__.py
│   ├── lr_schedule.py  # LR scheduler
│   ├── mlflow_funcs.py  # Aux tools to log in MLFlow
│   └── net_with_loss.py  # Wrapper for network with loss
├── convert_dataset.py  # Script for converting dataset to Mindrecord
├── eval.py  # Evaluation script
├── infer.py  # Inference script
├── train.py  # Train script
└── README.md
```

### [Script Parameters](#contents)

Major parameters in the yaml config file as follows:

```yaml
# Builtin Configurations(DO NOT CHANGE THESE CONFIGURATIONS unless you know exactly what you are doing)
enable_modelarts: False
data_url: ""
train_url: "/cache/data/foveabox/"
checkpoint_url: ""
data_path: "/cache/data"
output_path: "/cache/train"
load_path: "/cache/checkpoint_path"
enable_profiling: False

train_outputs: './train_outputs'
brief: 'gpu-1_1024x1024'
device_target: GPU
# ==============================================================================
# backbone
backbone:
  type: "resnet"
  depth: 50
  pretrained: './checkpoints/resnet50.ckpt'
  frozen_stages: 1
  norm_eval: True
  num_stages: 4
  out_indices: [0, 1, 2, 3]

# neck
neck:
  fpn:
    in_channels: [256, 512, 1024, 2048]
    out_channels: 256
    start_level: 1
    num_outs: 5
    add_extra_convs: 'on_input'


# bbox_head
bbox_head:
  type: 'FoveaHead'
  num_classes: 80
  in_channels: 256
  stacked_convs: 4
  feat_channels: 256
  strides: [8, 16, 32, 64, 128]
  base_edge_list: [16, 32, 64, 128, 256]
  scale_ranges: [[1, 64], [32, 128], [64, 256], [128, 512], [256, 2048]]
  sigma: 0.4
  with_deform: False
  loss_cls: {
    type: 'FocalLoss',
    use_sigmoid: True,
    gamma: 1.5,
    alpha: 0.4,
    loss_weight: 1.0
  }
  loss_bbox: {
    type: 'SmoothL1Loss',
    beta: 0.11,
    loss_weight: 1.0
  }

test_cfg:
  nms_pre: 1000
  score_thr: 0.05
  nms:
    type: 'nms'
    iou_threshold: 0.5
  max_per_img: 100

# images
img_width: 1024
img_height: 1024
divider: 64
img_mean: [123.675, 116.28, 103.53]
img_std: [58.395, 57.12, 57.375]
to_rgb: 1
keep_ratio: 1

# train
num_gts: 100
batch_size: 4
test_batch_size: 1
loss_scale: 256
epoch_size: 3
run_eval: 1
eval_every: 1
enable_graph_kernel: 0
finetune: 0
datasink: 0
pre_trained: ''

#distribution training
run_distribute: False
device_id: 0
device_num: 1
rank_id: 0

# Number of threads used to process the dataset in parallel
num_parallel_workers: 2
# Parallelize Python operations with multiple worker processes
python_multiprocessing: False

# dataset setting
train_data_type: 'coco'
val_data_type: 'mindrecord'
train_dataset: '/data/coco-2017/train/'
val_dataset: '/data/coco-2017/validation'
coco_classes: ['background', 'person', 'bicycle', 'car', 'motorcycle',
               'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']
train_dataset_num: 0
train_dataset_divider: 0

# optimizer
opt_type: "sgd"
lr: 0.001
min_lr: 0.000001
momentum: 0.9
weight_decay: 0.0001
warmup_step: 500
warmup_ratio: 0.001
lr_steps: [8, 11]
lr_type: "multistep"
grad_clip: 10

# augmentation
flip_ratio: 0.5
expand_ratio: 0.0


# callbacks
save_every: 100
keep_checkpoint_max: 5
keep_best_checkpoints_max: 5
print_loss_every: 1

pynative_mode: False
```

## [Training](#contents)

To train the model, Python script `train.py` may be used.
Also, there are two bash scripts for standalone and distributed training:

* `scripts/run_standalone_train_gpu.sh`
* `scripts/run_distribute_train_gpu.sh`

### [Training process](#contents)

Standalone training mode:

```bash
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]
```

* `CONFIG_PATH`: path to config file.
* `TRAIN_DATA`: path to train data.
* `VAL_DATA`: path to validation data.
* `TRAIN_OUT`: directory to store training results.
* `BRIEF`: comment to add to directory name.
* `PRETRAINED_PATH`: (optional) path to pretrained checkpoint.

Training result will be stored in the path passed in config option `train_outputs`

## [Evaluation](#contents)

### [Evaluation process](#contents)

#### [Evaluation on GPU](#contents)

```shell
bash scripts/run_eval_gpu.sh [CONFIG_PATH] [VAL_DATA] [CHECKPOINT_PATH] (Optional)[PREDICTION_PATH]
```

* `CONFIG_PATH`: path to config file.
* `VAL_DATA`: path to checkpoint.
* `CHECKPOINT_PATH`: path to checkpoint.
* `PREDICTION_PATH`: (optional) path to file with predictions.

### [Evaluation result](#contents)

Result for GPU:

```log
CHECKING MINDRECORD FILES DONE!
Start Eval!
loading annotations into memory...
Done (t=0.59s)
creating index...
index created!

========================================

total images num:  5000
Processing, please wait a moment.
100%|███████████████| 5000/5000 [07:48<00:00, 10.68it/s]
Loading and preparing results...
Converting ndarray to lists...
(308092, 7)
0/308092
DONE (t=2.58s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=41.48s).
Accumulating evaluation results...
DONE (t=9.14s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.395
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.604
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.421
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.227
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.440
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.520
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.328
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.523
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.553
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.352
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.604
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.706
Eval result: 0.3952826661588523

Evaluation done!

Done!
Time taken: 527 seconds
```

## [Model Description](#contents)

### [Performance](#contents)

| Parameters          | GPU                                                          |
| ------------------- |--------------------------------------------------------------|
| Model Version       | Foveabox Resnet-50 align mstrain                             |
| Resource            | NVIDIA GeForce RTX 3090 (x4)                                 |
| Uploaded Date       | 11/10/2023 (month/day/year)                                  |
| MindSpore Version   | 2.1.0                                                        |
| Dataset             | COCO2017                                                     |
| Pretrained          | noised checkpoint (start mAP=38.96%)                         |
| Training Parameters | epoch = 4, batch_size = 4 (per device)                       |
| Optimizer           | SGD (momentum)                                               |
| Loss Function       | FocalLoss, SmoothL1Loss                                      |
| Speed               | 863 ms / step                                                |
| Total time          | 7h h 17 min                                                  |
| outputs             | mAP                                                          |
| mAP                 | 0.395                                                        |
| Model for inference | 431.5 (.ckpt file)                                           |
| configuration       | fovea_align_r50_fpn_gn-head_mstrain_640-800_4x4_2x_coco.yaml |
| Scripts             |                                                              |

## [Description of Random Situation](#contents)

We use random seed in train.py.

## [ModelZoo Homepage](#contents)

Please check the official [homepage](https://gitee.com/mindspore/models).
