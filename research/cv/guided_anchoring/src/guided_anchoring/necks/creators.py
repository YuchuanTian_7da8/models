# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from ...config import Config
from .fpn import FPN


def create_neck(config: Config) -> FPN:
    if config.type == 'FPN':
        return FPN(
            in_channels=config.in_channels,
            out_channels=config.out_channels,
            num_outs=config.num_outs,
            start_level=config.get('start_level', 0),
            end_level=config.get('end_level', -1),
            add_extra_convs=config.get('add_extra_convs', False),
        )
    raise RuntimeError(f'Unknown model type: {config.type}')
