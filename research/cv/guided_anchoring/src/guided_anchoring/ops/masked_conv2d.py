# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmcv/tree/v1.7.1
# repository and modified.
# ============================================================================
import os
from typing import Optional, Tuple, Union

import mindspore as ms
import mindspore.nn as nn
import mindspore.numpy as np
import mindspore.ops as ops


@ops.constexpr
def calc_shapes(features_shape, weight_shape, padding, stride):
    _, _, kernel_h, kernel_w = weight_shape

    out_h = int(np.floor(
        np.true_divide((features_shape[2] + 2 * padding -
                        (kernel_h - 1) - 1), stride) + 1,
        dtype=ms.int32))
    out_w = int(np.floor(
        np.true_divide((features_shape[3] + 2 * padding -
                        (kernel_w - 1) - 1), stride) + 1,
        dtype=ms.int32))

    return out_w, out_h


class MaskedConv2d(nn.Conv2d):
    """
    A MaskedConv2d which inherits the official Conv2d.

    The masked forward doesn't implement the backward function and only
    supports the stride parameter to be 1 currently.
    """

    def __init__(self,
                 in_channels: int,
                 out_channels: int,
                 kernel_size: Union[int, Tuple[int, ...]],
                 stride: int = 1,
                 pad_mode='valid',
                 padding: int = 0,
                 dilation: int = 1,
                 group: int = 1,
                 has_bias: bool = True):
        super().__init__(in_channels, out_channels, kernel_size, stride,
                         pad_mode, padding, dilation, group, has_bias)

        self._padding = padding
        self._stride = stride

        self.matmul = ops.MatMul()

        (
            self.out_channel, self.in_channel, self.kernel_h_int,
            self.kernel_w_int
        ) = self.weight.shape

        self.kernel_h = ms.Tensor(self.kernel_h_int, ms.int32)
        self.kernel_w = ms.Tensor(self.kernel_w_int, ms.int32)
        self.pad = ms.Tensor(padding, ms.int32)

        self.masked_im2col_forward = ms.ops.Custom(

            os.path.join(
                os.path.dirname(__file__), 'bin',
                'masked_conv2d.so:CustomMaskedIm2colForward'
            ),
            out_shape=self.get_out_shape_im2col,
            out_dtype=ms.float32,
            func_type='aot'
        )
        self.masked_col2im_forward = ms.ops.Custom(

            os.path.join(
                os.path.dirname(__file__), 'bin',
                'masked_conv2d.so:CustomMaskedCol2imForward'
            ),

            out_shape=self.get_out_shape_col2im,
            out_dtype=ms.float32,
            func_type='aot',
        )

        self.identity = nn.Identity()

    def get_out_shape_im2col(self, inputs, *x):
        out_w, out_h = calc_shapes(
            inputs, self.weight.shape, self._padding, self._stride
        )
        return (
            (self.in_channel * self.kernel_h_int * self.kernel_w_int,
             out_h * out_w)
        )

    def get_out_shape_col2im(self, inputs, *x):
        out_w, out_h = calc_shapes(
            inputs, self.weight.shape, self._padding, self._stride
        )
        return (
            (1, self.out_channel, out_h, out_w)
        )

    def construct(self,
                  inp: ms.Tensor,
                  mask: Optional[ms.Tensor] = None) -> ms.Tensor:

        if mask is None:  # fallback to the normal Conv2d
            return super().construct(inp)

        out_w, out_h = calc_shapes(
            inp.shape, self.weight.shape, self._padding, self._stride
        )

        result = ops.zeros(
            (inp.shape[0], self.out_channel, out_h, out_w),
            inp.dtype)
        for i in range(inp.shape[0]):

            new_input = ops.zeros(
                (1, inp.shape[1], inp.shape[2], inp.shape[3]),
                inp.dtype)

            new_input[0] = inp[i]
            new_mask = ops.zeros(
                (1, mask.shape[1], mask.shape[2]),
                inp.dtype)

            new_mask[0] = mask[i]

            result[i] = self.masked_conv2d(
                new_input, new_mask, self.weight, self.bias
            )[0]

        return result

    def masked_conv2d(self,
                      features: ms.Tensor,
                      mask: ms.Tensor,
                      weight: ms.Parameter,
                      bias: ms.Parameter,
                      ) -> ms.Tensor:

        out_w, out_h = calc_shapes(
            features.shape, weight.shape, self._padding, self._stride
        )

        mask_inds = (mask[0] > 0).nonzero()
        flatten_mask = ops.flatten(mask)
        indexes = np.arange(0, features.shape[2] * features.shape[3])
        masked_indexes = ops.masked_select(indexes, flatten_mask > 0)

        mask_inds_2 = ms.ops.fill(ms.int64, (out_h * out_w, 2), -1)
        if masked_indexes.shape[0] > 0:
            mask_inds_2[:, 0][masked_indexes] = mask_inds[:, 0]
            mask_inds_2[:, 1][masked_indexes] = mask_inds[:, 1]

        mask_h_idx = mask_inds_2[:, 0]
        mask_w_idx = mask_inds_2[:, 1]

        data_col = self.masked_im2col_forward(
            features,
            mask_h_idx,
            mask_w_idx,
            self.kernel_w,
            self.kernel_h,
            self.pad
        )
        masked_output = (
            bias[:, None]
            + self.matmul(weight.view(self.out_channel, -1), data_col)
        )

        output = self.masked_col2im_forward(
            features,
            mask_h_idx,
            mask_w_idx,
            masked_output,
        )

        return output
