# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmcv/tree/v1.7.1
# repository and modified.
# ============================================================================
"""ROIAlign module."""
import mindspore as ms
from mindspore import nn, ops


class ROIAlign(nn.Cell):

    def __init__(
            self,
            output_size=7,
            spatial_scale=1.0,
            sampling_ratio=0,
            pool_mode='avg',
            aligned=True
    ):
        super().__init__()
        self.output_size = output_size
        self.sampling_ratio = sampling_ratio

        self.pooled_height = ms.Tensor(output_size, ms.int32)
        self.pooled_width = ms.Tensor(output_size, ms.int32)
        self.spatial_scale = ms.Tensor(spatial_scale, ms.float32)
        self.sampling_ratio = ms.Tensor(sampling_ratio, ms.int32)

        if pool_mode == 'avg':
            self.pool_mode = ms.Tensor(1, ms.int32)
        elif pool_mode == 'max':
            self.pool_mode = ms.Tensor(0, ms.int32)
        else:
            raise ValueError(
                f'pool_mode must be in{("avg", "max")} but {pool_mode} was '
                f'obtained.'
            )

        self.aligned = ms.Tensor(1 if aligned else 0, ms.int32)

        self.forward_op = ops.Custom(
            './src/guided_anchoring/ops/bin/roi_align.so:'
            'CustomROIAlign',
            out_shape=self.get_out_shape,
            out_dtype=self.get_out_type,
            bprop=self.backward(),
            func_type='aot'
        )

    def backward(self):
        backward_op = ops.Custom(
            './src/guided_anchoring/ops/bin/roi_align.so:'
            'CustomROIAlignBackward',
            self.get_out_shape_backward,
            self.get_out_type_backward,
            func_type='aot'
        )

        def custom_bprop(
                features,
                rois,
                pooled_height,
                pooled_width,
                spatial_scale,
                sampling_ratio,
                pool_mode,
                aligned,
                outputs,
                dout
        ):
            outputs, argmax_y, argmax_x = outputs
            grad_output = dout[0]
            df = backward_op(
                features,
                rois,
                argmax_y,
                argmax_x,
                grad_output,
                pooled_height,
                pooled_width,
                spatial_scale,
                sampling_ratio,
                pool_mode,
                aligned,
            )
            dr = ms.ops.zeros_like(rois)
            return df, dr, 0., 0., 0., 0., 0., 0.

        return custom_bprop

    def get_out_shape(self, inputs, rois_, *x):
        return (
            (rois_[0], inputs[1], self.pooled_height, self.pooled_width),
            (rois_[0], inputs[1], self.pooled_height, self.pooled_width),
            (rois_[0], inputs[1], self.pooled_height, self.pooled_width),
        )

    @staticmethod
    def get_out_type(inputs, rois_, *x):
        return ms.float32, ms.float32, ms.float32

    @staticmethod
    def get_out_shape_backward(features, *x):
        return features

    @staticmethod
    def get_out_type_backward(features, *x):
        return features

    def construct(self, features, rois):
        roi_feats, _, _ = self.forward_op(
            features,
            rois,
            self.pooled_height,
            self.pooled_width,
            self.spatial_scale,
            self.sampling_ratio,
            self.pool_mode,
            self.aligned
        )
        return roi_feats
