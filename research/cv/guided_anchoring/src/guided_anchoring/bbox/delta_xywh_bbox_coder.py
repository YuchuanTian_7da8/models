# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
import mindspore as ms
import mindspore.ops as ops
from mindspore import nn


class DeltaXYWHBBoxCoder(nn.Cell):
    """
    Delta XYWH BBox coder.
    Following the practice in `R-CNN <https://arxiv.org/abs/1311.2524>`_,
    this coder encodes bbox (x1, y1, x2, y2) into delta (dx, dy, dw, dh) and
    decodes delta (dx, dy, dw, dh) back to original bbox (x1, y1, x2, y2).

    Args:
    ----
        target_means (Sequence[float]): Denormalizing means of target for
            delta coordinates
        target_stds (Sequence[float]): Denormalizing standard deviation of
            target for delta coordinates
        clip_border (bool, optional): Whether clip the objects outside the
            border of the image. Defaults to True.
        add_ctr_clamp (bool): Whether to add center clamp, when added, the
            predicted box is clamped is its center is too far away from
            the original anchor's center. Only used by YOLOF. Default False.
        ctr_clamp (int): the maximum pixel shift to clamp. Only used by YOLOF.
            Default 32.
    """

    def __init__(self,
                 target_means=(0., 0., 0., 0.),
                 target_stds=(1., 1., 1., 1.),
                 clip_border=True,
                 add_ctr_clamp=False,
                 ctr_clamp=32):
        super().__init__()
        self.means = ms.Tensor(target_means, ms.float32).reshape(1, 4)
        self.stds = ms.Tensor(target_stds, ms.float32).reshape(1, 4)
        self.clip_border = clip_border
        self.add_ctr_clamp = add_ctr_clamp
        self.ctr_clamp = ctr_clamp
        self.eps = ms.Tensor(1e-10, ms.float32)
        self.wp_ratio_clip = ms.Tensor(0.016)

    # @ms.jit()
    def encode(self, bboxes, gt_bboxes):
        """
        Get box regression transformation deltas that can be used to
        transform the ``bboxes`` into the ``gt_bboxes``.

        Args:
        ----
            bboxes (mindspore.Tensor): Source boxes, e.g., object proposals.
            gt_bboxes (mindspore.Tensor): Target of the transformation, e.g.,
                ground-truth boxes.

        Returns:
        -------
            mindspore.Tensor: Box transformation deltas.
        """
        assert bboxes.shape[0] == gt_bboxes.shape[0]
        assert bboxes.shape[-1] == 4 and gt_bboxes.shape[-1] == 4
        encoded_bboxes = self.bbox2delta(bboxes, gt_bboxes)
        return encoded_bboxes

    # @ms.jit()
    def decode(self,
               bboxes,
               pred_bboxes,
               max_shape=None,
               wh_ratio_clip=None):
        """
        Apply transformation `pred_bboxes` to `boxes`.

        Args:
        ----
            bboxes (mindspore.Tensor): Basic boxes. Shape (B, N, 4) or (N, 4)
            pred_bboxes (mindspore.Tensor): Encoded offsets with respect to
                each roi. Has shape (B, N, num_classes * 4) or (B, N, 4) or
                (N, num_classes * 4) or (N, 4). Note N = num_anchors * W * H
                when rois is a grid of anchors.Offset encoding follows [1]_.
            max_shape (Sequence[int] or mindspore.Tensor or Sequence[
                Sequence[int]],optional): Maximum bounds for boxes, specifies
                (H, W, C) or (H, W). If bboxes shape is (B, N, 4), then
                the max_shape should be a Sequence[Sequence[int]]
                and the length of max_shape should also be B.
            wh_ratio_clip (float, optional): The allowed ratio between
                width and height.

        Returns:
        -------
            mindspore.Tensor: Decoded boxes.
        """
        if not wh_ratio_clip:
            wh_ratio_clip = self.wp_ratio_clip

        decoded_bboxes = self.delta2bbox(
            bboxes, pred_bboxes, max_shape,
            wh_ratio_clip, self.clip_border, self.add_ctr_clamp, self.ctr_clamp
        )

        return decoded_bboxes

    def bbox2delta(self, proposals, gt):
        """
        Compute deltas of proposals w.r.t. gt.
        We usually compute the deltas of x, y, w, h of proposals w.r.t ground
        truth bboxes to get regression target.
        This is the inverse function of :func:`delta2bbox`.

        Args:
        ----
            proposals (mindspore.Tensor): Boxes to be transformed, shape
                (N, ..., 4)
            gt (mindspore.Tensor): Gt bboxes to be used as base, shape
                (N, ..., 4)

        Returns:
        -------
            mindspore.Tensor: deltas with shape (N, 4), where columns represent
                dx, dy, dw, dh.
        """
        assert proposals.shape == gt.shape

        proposals = ops.cast(proposals, ms.float32)
        gt = ops.cast(gt, ms.float32)
        px = (proposals[::, 0] + proposals[::, 2]) * 0.5
        py = (proposals[::, 1] + proposals[::, 3]) * 0.5
        pw = proposals[::, 2] - proposals[::, 0] + self.eps
        ph = proposals[::, 3] - proposals[::, 1] + self.eps
        gx = (gt[::, 0] + gt[::, 2]) * 0.5
        gy = (gt[::, 1] + gt[::, 3]) * 0.5
        gw = gt[::, 2] - gt[::, 0] + self.eps
        gh = gt[::, 3] - gt[::, 1] + self.eps

        dx = (gx - px) / pw
        dy = (gy - py) / ph
        dw = ops.log(gw / pw)
        dh = ops.log(gh / ph)
        deltas = ops.stack([dx, dy, dw, dh], axis=-1)

        return deltas

    def delta2bbox(
            self,
            rois,
            deltas,
            max_shape=None,
            wh_ratio_clip=0.016,
            clip_border=True,
            add_ctr_clamp=False,
            ctr_clamp=32
    ):
        """
        Apply deltas to shift/scale base boxes.
        Typically the rois are anchor or proposed bounding boxes and the deltas
        are network outputs used to shift/scale those boxes.
        This is the inverse function of :func:`bbox2delta`.

        Args:
        ----
            rois (Tensor): Boxes to be transformed. Has shape (N, 4).
            deltas (Tensor): Encoded offsets relative to each roi.
                Has shape (N, num_classes * 4) or (N, 4). Note
                N = num_base_anchors * W * H, when rois is a grid of
                anchors. Offset encoding follows [1]_.
            max_shape (tuple[int, int]): Maximum bounds for boxes, specifies
               (H, W). Default None.
            wh_ratio_clip (float): Maximum aspect ratio for boxes. Default
                16 / 1000.
            clip_border (bool, optional): Whether clip the objects outside the
                border of the image. Default True.
            add_ctr_clamp (bool): Whether to add center clamp. When set to True,
                the center of the prediction bounding box will be clamped to
                avoid being too far away from the center of the anchor.
                Only used by YOLOF. Default False.
            ctr_clamp (int): the maximum pixel shift to clamp. Only used by YOLOF.
                Default 32.

        Returns:
        -------
            Tensor: Boxes with shape (N, num_classes * 4) or (N, 4), where 4
               represent tl_x, tl_y, br_x, br_y.
        """
        means = self.means
        stds = self.stds

        num_bboxes = deltas.shape[0]
        if num_bboxes == 0:
            return deltas

        num_classes = deltas.shape[1] // 4

        deltas = ops.reshape(deltas, (-1, 4))

        denorm_deltas = deltas * stds + means

        dxy = denorm_deltas.T[:2].T
        dwh = denorm_deltas.T[2:].T

        # Compute width/height of each roi
        rois_ = ops.tile(rois, (1, num_classes)).reshape(-1, 4)
        pxy = ((rois_.T[:2] + rois_.T[2:]).T * 0.5)
        pwh = (rois_.T[2:] - rois_.T[:2]).T

        dxy_wh = pwh * dxy

        max_ratio = ops.abs(ops.log(wh_ratio_clip))

        if add_ctr_clamp:
            dxy_wh = ops.clip_by_value(
                dxy_wh, clip_value_min=-ctr_clamp, clip_value_max=ctr_clamp
            )
            dwh = ops.clip_by_value(dwh, clip_value_max=max_ratio)
        else:
            ops.masked_fill(dwh, dwh < -max_ratio, -max_ratio)
            ops.masked_fill(dwh, dwh > max_ratio, max_ratio)

        gxy = pxy + dxy_wh
        gwh = pwh * ops.exp(dwh)
        x1y1 = gxy - (gwh * 0.5)
        x2y2 = gxy + (gwh * 0.5)
        bboxes = ops.hstack([x1y1, x2y2])
        bboxes = bboxes.reshape((num_bboxes, -1))
        return bboxes
