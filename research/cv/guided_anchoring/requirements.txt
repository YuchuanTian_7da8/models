mindspore~=2.1.0
numpy~=1.21.6
onnxruntime~=1.13.1
opencv-python~=4.8.1
pycocotools~=2.0.7
PyYAML~=6.0
seaborn~=0.12.2
tqdm~=4.64.1
