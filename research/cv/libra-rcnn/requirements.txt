Cython~=0.28.5
mindspore-dev==2.0.0.dev20230609
numpy~=1.21.2
opencv-python~=4.5.4.58
pycocotools>=2.0.5
matplotlib
seaborn
pandas
tqdm==4.64.1
