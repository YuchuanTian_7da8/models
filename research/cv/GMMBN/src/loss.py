import numpy as np
import mindspore
from mindspore import Tensor
import mindspore.ops as ops
import mindspore.nn as nn
from utils.loss_registry import is_loss, loss_entrypoint, register_loss

def get_loss(args):
    if is_loss(args.loss_type):
        loss_handler = loss_entrypoint(args.loss_type)
        return loss_handler(args)
    return None

@register_loss
def CrossEntropy(args):
    weight = get_weights_by_cls_num(args.cls_num_list) if args.re_weighting else None
    return nn.CrossEntropyLoss(weight=weight)

def get_weights_by_cls_num(cls_num_list):
    per_cls_weights = 1.0 / np.asarray(cls_num_list)
    per_cls_weights = per_cls_weights /  np.sum(per_cls_weights)
    return per_cls_weights



@register_loss
def balanced_softmax_loss(labels, logits, sample_per_class, reduction='mean'):
    """Compute the Balanced Softmax Loss between `logits` and the ground truth `labels`.
    Args:
      labels: A int tensor of size [batch].
      logits: A float tensor of size [batch, no_of_classes].
      sample_per_class: A int tensor of size [no of classes].
      reduction: string. One of "none", "mean", "sum"
    Returns:
      loss: A float tensor. Balanced Softmax Loss.
    """
    sample_per_class = Tensor.from_numpy(np.asarray(sample_per_class))
    spc = sample_per_class.astype(logits.dtype)
    spc = ops.broadcast_to(ops.expand_dims(spc, axis=0), (logits.shape[0], -1))
    logits = logits + ops.log(spc)
    loss = nn.SoftmaxCrossEntropyWithLogits(sparse=True, reduction=reduction)\
        (logits, Tensor(labels).astype(mindspore.int32))
    return loss

@register_loss
class SupervisedContrastiveLoss(nn.Cell):
    def __init__(self, temperature=0.07):
        super(SupervisedContrastiveLoss, self).__init__()
        self.temperature = temperature

    def forward(self, projections, targets):
        """
        :param projections: torch.Tensor, shape [batch_size, projection_dim]
        :param targets: torch.Tensor, shape [batch_size]
        :return: torch.Tensor, scalar
        """

        dot_product_tempered = ops.MatMul()(projections, projections.T) / self.temperature
        # Minus max for numerical stability with exponential. Same done in cross entropy. Epsilon added to avoid log(0)
        exp_dot_tempered = (
            ops.exp(dot_product_tempered - ops.ArgMaxWithValue(axis=1, keep_dims=True)(dot_product_tempered)[1]) + 1e-5
        )

        mask_similar_class = mindspore.numpy.tile(ops.expand_dims(targets, 1), (1, targets.shape[0])) == targets
        mask_anchor_out = (1 - ops.eye(exp_dot_tempered.shape[0]))
        mask_combined = mask_similar_class * mask_anchor_out
        cardinality_per_samples = ops.ReduceSum()(mask_combined, axis=1)

        log_prob = -ops.log(exp_dot_tempered / (ops.ReduceSum(keepdim=True)\
                                                (exp_dot_tempered * mask_anchor_out, axis=1)))
        supervised_contrastive_loss_per_sample = ops.ReduceSum()\
            (log_prob * mask_combined, axis=1) / cardinality_per_samples
        supervised_contrastive_loss = ops.ReduceMean()(supervised_contrastive_loss_per_sample)

        return supervised_contrastive_loss


def mixup_data(x, y, alpha=1.0, use_cuda=True):
    '''Returns mixed inputs, pairs of targets, and lambda'''
    if alpha > 0:
        lam = np.random.beta(alpha, alpha)
    else:
        lam = 1
    batch_size = x.shape()[0]
    index = ops.Randperm(max_length=batch_size)(batch_size)
    mixed_x = lam * x + (1 - lam) * x[index, :]
    y_a, y_b = y, y[index]
    return mixed_x, y_a, y_b, lam

def mixup_criterion(criterion, pred, y_a, y_b, lam):
    return lam * criterion(pred, y_a) + (1 - lam) * criterion(pred, y_b)


def focal_loss(input_values, gamma):
    """Computes the focal loss"""
    p = ops.exp(-input_values)
    loss = (1 - p) ** gamma * input_values
    return loss.mean()

@register_loss
class FocalLoss(nn.Cell):
    def __init__(self, args):
        super(FocalLoss, self).__init__()
        self.args = args
        #assert gamma >= 0
        self.gamma = args.gamma if hasattr(args, 'gamma') else 2

    def construct(self, inputs, target):
        return focal_loss(nn.SoftmaxCrossEntropyWithLogits()(inputs, target), self.gamma)

@register_loss
class LDAMLoss(nn.Cell):
    def __init__(self, cls_num_list, max_m=0.5, weight=None, s=30):
        super(LDAMLoss, self).__init__()
        m_list = 1.0 / np.sqrt(np.sqrt(cls_num_list))
        m_list = m_list * (max_m / np.max(m_list))
        m_list = Tensor(m_list).astype(mindspore.float16)
        self.m_list = m_list
        assert s > 0
        self.s = s

    def construct(self, x, target):
        index = mindspore.numpy.zeros_like(x, dtype=mindspore.uint8)
        index.scatter_(1, target.data.view(-1, 1), 1)

        index_float = index.astype(mindspore.float16)
        batch_m = ops.MatMul()(self.m_list[None, :], index_float.transpose(0, 1))
        batch_m = batch_m.view((-1, 1))
        x_m = x - batch_m
        output = mindspore.numpy.where(index, x_m, x)
        return nn.SoftmaxCrossEntropyWithLogits()(self.s*output, target)

@register_loss
class MatchingLoss(nn.Cell):
    def __init__(self, args, cls_num_list, weight=None, weak_t=None, strong_t=None, stage=True):
        super(MatchingLoss, self).__init__()
        self.args = args
        self.cls_num_list = Tensor(cls_num_list).astype(mindspore.float16)
        self.weight = weight

        self.weak_t = (1 - weak_t)/sum(1-weak_t) if weak_t is not None else None

        self.strong_t = (1 - strong_t)/sum(1-strong_t) if strong_t is not None else None

        self.weak_plus = Tensor.from_numpy(np.zeros(args.num_classes,))
        self.strong_plus = Tensor.from_numpy(np.zeros(args.num_classes,))

        #self.num = 0
        self.stage = stage

        self.criterion = LDAMLoss(cls_num_list=cls_num_list, max_m=0.5, s=30, weight=weight)

    def construct(self, logits1, logits2, noisy_targets, clean_targets, epoch=0):
        outputs1 = ops.Softmax(axis=1)(logits1)
        outputs2 = ops.softmax(axis=1)(logits2)

        _, pred1 = ops.ArgMaxWithValue(axis=1)(logits1.data)

        if self.stage is True: ## stage 1
            #online prior update
            self.weak_plus = self.weak_plus + outputs1.sum(axis=0)
            self.strong_plus = self.strong_plus + outputs2.sum(axis=0)

            # stage 1:
            loss1 = self.criterion(logits1, noisy_targets)
            loss2 = self.criterion(logits2, noisy_targets)

            #if epoch < self.args.warm_up:
            #    alpha = (epoch - self.args.start_epoch) / (self.args.warm_up - self.args.start_epoch)
            #elif epoch > self.args.warm_up:
            #    alpha = (epoch - self.args.warm_up) / (self.args.epochs - self.args.warm_up)
            #else:
            #    alpha = 0.5

            loss = self.args.noise_rate * loss1 + (1 - self.args.noise_rate) * loss2
            #loss = alpha * loss1 +  (1 - alpha) * loss2
            #loss = loss1 + loss2

            #loss = loss1 + loss2
            if self.weak_t is not None:
                w_t = self.weak_t.reshape(1, -1).expand_as(logits1)
                s_t = self.strong_t.reshape(1, -1).expand_as(logits2)

                loss += 0.1 * ops.ReduceMean()(ops.ReduceSum()(-nn.LogSoftmax(axis=-1)(logits1) * w_t, axis=1))
                loss += 0.1 * ops.ReduceMean()(ops.ReduceSum()(-nn.LogSoftmax(axis=-1)(logits2) * s_t, axis=1))

        else: ## stage 2
            mloss = nn.SoftmaxCrossEntropyWithLogits(reduction='none')(logits2, pred1) \
                    + 0.5 * nn.SoftmaxCrossEntropyWithLogits(reduction='none')(logits1, noisy_targets)\
                    + 0.5 * nn.SoftmaxCrossEntropyWithLogits(reduction='none')(logits2, noisy_targets)\

            if self.args.adp is True:
                ml = mloss.asnumpy()
                th = nthresh(ml, n_classes=5, bins=10, n_jobs=1)
                ind_clean = ml <= th[-1]
                ind_noise = ml > th[-1]

            else:

                ind_sorted = np.argsort(mloss.asnumpy())
                loss_sorted = mloss[ind_sorted]
                clean_num = int((1 - self.args.noise_rate) * len(loss_sorted))
                ind_clean = ind_sorted[:clean_num]
                ind_noise = ind_sorted[clean_num:]

            #online prior update
            self.weak_plus = self.weak_plus + outputs1[ind_clean].sum(axis=0)
            self.strong_plus = self.strong_plus + outputs2[ind_clean].sum(axis=0)

            # predicted clean sample
            loss1 = self.criterion(logits1[ind_clean], noisy_targets[ind_clean])
            loss2 = self.criterion(logits2[ind_clean], noisy_targets[ind_clean])

            loss = self.args.noise_rate * loss1 + (1 - self.args.noise_rate) * loss2

            if self.weak_t is not None:
                w_t_c = self.weak_t.reshape(1, -1).expand_as(logits1)[ind_clean]
                s_t_c = self.strong_t.reshape(1, -1).expand_as(logits2)[ind_clean]

                loss += 0.1*ops.ReduceMean()(ops.ReduceSum()\
                    (-nn.LogSoftmax(axis=1)(logits1[ind_clean]) * w_t_c, axis=1))
                loss += 0.1*ops.ReduceMean()(ops.ReduceSum()\
                    (-nn.LogSoftmax(axis=1)(logits2[ind_clean]) * s_t_c, axis=1))

                t_n = (1-ops.OneHot()(noisy_targets, logits1.shape[1]))[ind_noise]
                loss += 0.1*ops.ReduceMean()(ops.ReduceSum()(-nn.LogSoftmax(axis=-1)(logits1[ind_noise]) * t_n, axis=1))
                loss += 0.1*ops.ReduceMean()(ops.ReduceSum()(-nn.LogSoftmax(axis=-1)(logits2[ind_noise]) * t_n, axis=1))

        return loss

@register_loss
class SlackPlusLoss(nn.Cell):
    def __init__(self, cls_num_list, max_m=0.5, weight=None, noise_rate=0, s=30, std=0.02, warm_up=10):
        super(SlackPlusLoss, self).__init__()
        m_list = 1.0 / np.sqrt(np.sqrt(cls_num_list))
        m_list = m_list * (max_m / np.max(m_list))
        m_list = Tensor(m_list).astype(mindspore.float16)
        self.m_list = m_list
        assert s > 0
        self.s = s
        self.weight = weight
        self.noise_rate = noise_rate
        self.warm_up = warm_up
        self.num = 0
        self.std = std

    def slack(self, x, target):
        index = mindspore.numpy.zeros_like(x, dtype=mindspore.uint8)
        index.scatter_(1, target.data.view(-1, 1), 1)

        index_float = index.astype(mindspore.float16)

        batch_m = ops.MatMul()(self.m_list[None, :], index_float.transpose(0, 1))
        batch_m = batch_m.view((-1, 1))

        #shape如何确定
        slack = ops.normal(shape=batch_m, std=self.std)

        x_m = x - slack

        output = mindspore.numpy.where(index, x_m, x)
        logits = self.s*output

        return logits

    def construct(self, x1, x2, target):

        logits1 = self.slack(x1, target)
        logits2 = self.slack(x2, target)


        if self.num < self.warm_up:
            ## noise_rate * weak_loss + (1 - noise_rate) * strong_loss
            weak_loss = nn.SoftmaxCrossEntropyWithLogits()(logits1, target)
            strong_loss = nn.SoftmaxCrossEntropyWithLogits()(logits2, target)
            output = self.noise_rate * weak_loss + (1 - self.noise_rate) * strong_loss
        else:

            ## weak augment loss --> select samples below threshold
            loss1 = nn.SoftmaxCrossEntropyWithLogits(reduction='none')(logits1, target)
            ind1_sorted = np.argsort(loss1.asnumpy())
            loss1_sorted = loss1[ind1_sorted]
            keep_num1 = int((1 - self.noise_rate) * len(loss1_sorted))
            ind1_update = ind1_sorted[:keep_num1]

            ## strong augment loss --> select samples
            loss2 = nn.SoftmaxCrossEntropyWithLogits(reduction='none')(logits2, target)
            ind2_sorted = np.argsort(loss2.asnumpy())
            loss2_sorted = loss2[ind2_sorted]
            keep_num2 = int((1 - self.noise_rate) * len(loss2_sorted))
            ind2_update = ind2_sorted[:keep_num2]


            weak_loss = nn.SoftmaxCrossEntropyWithLogits()(logits1[ind2_update], target[ind2_update])
            strong_loss = nn.SoftmaxCrossEntropyWithLogits()(logits2[ind1_update], target[ind1_update])

            output = self.noise_rate * weak_loss + (1 - self.noise_rate) * strong_loss

        self.num += 1
        return output

@register_loss
def loss_coteaching(y_1, y_2, t, forget_rate):
    loss_1 = nn.SoftmaxCrossEntropyWithLogits(reduction='none')(y_1, t)
    ind_1_sorted = np.argsort(loss_1.asnumpy())
    loss_1_sorted = loss_1[ind_1_sorted]

    loss_2 = nn.SoftmaxCrossEntropyWithLogits(reduction='none')(y_2, t)
    ind_2_sorted = np.argsort(loss_2.asnumpy())

    remember_rate = 1 - forget_rate
    num_remember = int(remember_rate * len(loss_1_sorted))

    ind_1_update = ind_1_sorted[:num_remember]
    ind_2_update = ind_2_sorted[:num_remember]
    if ind_1_update:
        ind_1_update = ind_1_sorted.asnumpy()
        ind_2_update = ind_2_sorted.asnumpy()
        num_remember = ind_1_update.shape[0]

    loss_1_update = nn.SoftmaxCrossEntropyWithLogits()(y_1[ind_2_update], t[ind_2_update])
    loss_2_update = nn.SoftmaxCrossEntropyWithLogits()(y_2[ind_1_update], t[ind_1_update])

    return ops.ReduceSum()(loss_1_update)/num_remember, ops.ReduceSum()(loss_2_update)/num_remember

@register_loss
def loss_coteaching_plus(logits, logits2, labels, forget_rate):
    outputs = ops.Softmax(axis=1)(logits)
    outputs2 = ops.Softmax(axis=1)(logits2)

    _, pred1 = ops.ArgMaxWithValue(axis=1)(logits.data)
    _, pred2 = ops.ArgMaxWithValue(axis=1)(logits2.data)

    pred1, pred2 = pred1.asnumpy(), pred2.asnumpy()

    logical_disagree_id = np.zeros(labels.shape(), dtype=bool)
    disagree_id = []
    for idx, p1 in enumerate(pred1):
        if p1 != pred2[idx]:
            disagree_id.append(idx)
            logical_disagree_id[idx] = True

    if disagree_id:
        update_labels = labels[disagree_id]
        update_outputs = outputs[disagree_id]
        update_outputs2 = outputs2[disagree_id]

        loss_1, loss_2 = loss_coteaching(update_outputs, update_outputs2, update_labels, forget_rate)
    else:
        update_labels = labels
        update_outputs = outputs
        update_outputs2 = outputs2

        cross_entropy_1 = nn.SoftmaxCrossEntropyWithLogits()(update_outputs, update_labels)
        cross_entropy_2 = nn.SoftmaxCrossEntropyWithLogits()(update_outputs2, update_labels)
        loss_1 = ops.ReduceSum()(cross_entropy_1)/labels.shape()[0]
        loss_2 = ops.ReduceSum()(cross_entropy_2)/labels.shape()[0]

    return loss_1, loss_2

@register_loss
class SupConLoss(nn.Cell):
    def __init__(self, args):
        super(SupConLoss, self).__init__()

        self.args = args
        self.criterion = nn.BCEWithLogitsLoss()

    def construct(self, out1, label1, out2=None, label2=None):
        features1 = ops.L2Normalize(axis=1)(out1)
        if out2 is not None:
            features2 = ops.L2Normalize(axis=1)(out2)
        else:
            features2 = features1

        label1 = label1.view(-1, 1)
        if label2 is not None:
            label2 = label2.view(-1, 1)
        else:
            label2 = label1

        mask = ops.equal(label1, label2.T).astype(mindspore.float16)
        similarity = ops.MatMul()(features1, features2.T)
        loss = self.criterion(similarity, mask)
        return loss

@register_loss
class LabelSmoothing(nn.Cell):
    """
    NLL loss with label smoothing.
    """
    def __init__(self, smoothing=0.1):
        """
        Constructor for the LabelSmoothing module.
        :param smoothing: label smoothing factor
        """
        super(LabelSmoothing, self).__init__()
        self.confidence = 1.0 - smoothing
        self.smoothing = smoothing

    def construct(self, x, target):
        logprobs = nn.LogSoftmax(axis=-1)(x)
        nll_loss = -ops.gather_elements(logprobs, dim=-1, index=target.unsqueeze(1))
        nll_loss = ops.squeeze(nll_loss, 1)
        smooth_loss = -logprobs.mean(axis=-1)
        loss = self.confidence * nll_loss + self.smoothing * smooth_loss
        return loss.mean()

def get_one_hot(label, num_classes):
    batch_size = label.shape[0]
    onehot_label = ops.Zeros()((batch_size, num_classes))
    # 没有scatter_可替换的算子
    onehot_label = onehot_label.scatter_(1, ops.expand_dims(label, 1), 1)
    onehot_label = (onehot_label.astype(mindspore.float16))
    return onehot_label
