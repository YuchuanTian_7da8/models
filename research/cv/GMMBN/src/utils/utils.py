import os
import argparse
import yaml
from sklearn.metrics import confusion_matrix
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


import mindspore
import mindspore.ops as ops

amp = None
has_apex = False
matplotlib.use('Agg')

def get_args(known=False):

    config_parser = parser = argparse.ArgumentParser(description='config parser')
    parser.add_argument("--data_config", type=str, help="imbalance dataset setting")
    parser = argparse.ArgumentParser(description='Long-Tailed Learning')
    ## dataset setting
    parser.add_argument('--data_folder', default='/data', help='dataset folder')
    parser.add_argument('--log_folder', default='log/', help='log folder')
    parser.add_argument('--checkpoint_folder', type=str, default='checkpoint/', help='checkpoint folder')
    parser.add_argument('--excel', default="log/excel/results.xlsx", type=str)
    parser.add_argument('--data_name', default='cifar10', help='dataset folder')
    parser.add_argument('--data_ratio', default=0.01, type=float, help='imbalance factor')
    parser.add_argument('--data_seed', default=1024, type=float, help='seed for dataset')
    parser.add_argument('--download', action="store_true", default=True)
    parser.add_argument('--num_class', default=10, type=int, help='dataset number of class')
    parser.add_argument('--input_size', default=(32, 32), type=tuple, help='dataset input size')
    parser.add_argument('--input_ch', default=3, type=int, help="RGB or Grey Image")
    parser.add_argument('--pad_size', default=4, type=int, help='pad image size')
    parser.add_argument('--fill_value', default=128, type=int, help='image fill value')
    parser.add_argument('--val_size', default=32, type=int, help='dataset val size')
    parser.add_argument('--crop_size', default=32, type=int, help='dataset crop size')
    parser.add_argument('--cutout_num', default=1, type=int, help='num of cutout hole')
    parser.add_argument('--cutout_len', default=16, type=int, help='length of cutout hole')
    parser.add_argument('--img_mean', default=[0.4914, 0.4822, 0.4465], \
                        type=list, help='default image mean')
    parser.add_argument('--img_std', default=[0.2023, 0.1994, 0.2010], \
                        type=list, help='default image std')

    ## model setting
    parser.add_argument('--arch', default='resnet32', help='model architecture: (default: resnet32)')
    parser.add_argument('--bn_momentum', default=0.1, type=float, help='bn momentum')
    parser.add_argument('--ema_flag', action='store_true', default=False, help='expotential model average')
    parser.add_argument('--ema_decay', default=0.999, type=float, help='ema decay')
    parser.add_argument('--pretrained', action='store_true', default=False, help='use pre-trained model')
    parser.add_argument('--resume', action='store_true', default=False, help='if best checkpoint is used')
    parser.add_argument('--resume_checkpoint', type=str, default="")
    parser.add_argument('--distributed', action='store_true', default=False, help='parallel learning')
    parser.add_argument('--gpu', default=None, type=int, help='GPU id to use. conflict with distributed')
    parser.add_argument('--amp', action='store_true', default=False, help="apex amp mixed precision training.")
    parser.add_argument('--apex_amp', action='store_true', default=False, help="apex amp mixed precision training.")
    parser.add_argument('--native_amp', action='store_true', default=False, help="native amp mixed precision training.")
    parser.add_argument('--tta', type=int, default=0, help="Test Time Augmentation.")
    parser.add_argument('--local_rank', type=int, default=0)
    parser.add_argument('--split_bn', action='store_true', default=False, \
                        help="Enable separate BN layers per augmentation split.")


    ## optimization setting
    parser.add_argument('--batch_size', default=512, type=int, help='mini-batch size')
    parser.add_argument('--epochs', default=100, type=int, help='number of total epochs to run')
    parser.add_argument('--workers', default=8, type=int, help='number of data loading workers (default: 4)')
    parser.add_argument('--drop_last', action='store_true', default=False)
    parser.add_argument('--pip_memory', action='store_true', default=True)
    parser.add_argument('--optim_type', type=str, default="SGD", help="SGD")
    parser.add_argument('--clip_value', type=float, default=0.5)
    parser.add_argument('--lr', default=0.1, type=float, help='initial learning rate')
    parser.add_argument('--lsr', action='store_true', default=True, help='linear scaling rate')
    parser.add_argument('--warm_up', default=50, type=int)
    parser.add_argument('--weight_decay', default=2e-4, type=float, help='weight decay (default: 2e-4)')
    parser.add_argument('--momentum', default=0.9, type=float, help='sgd momentum')
    parser.add_argument('--start-epoch', default=0, type=int, help='manual epoch number (useful on restarts)')
    parser.add_argument('--epoch_interval', default=20, type=int, help='epoch_interval (default: 30)')
    parser.add_argument('--print-freq', default=10, type=int, help='print frequency (default: 10)')
    parser.add_argument('--k', type=int, default=4, help="number of bn for each raw bn")
    parser.add_argument('--comment', type=str, default="", help="additional comment")
    parser.add_argument('--loss_type', type=str, default="CrossEntropy", help="loss function")

    parser.add_argument('--re_sampling', action='store_true', default=False, help='linear scaling rate')
    parser.add_argument('--re_weighting', action='store_true', default=False, help='linear scaling rate')
    parser.add_argument('--freze_ratio', default=0.8, type=float, help='sgd momentum')

    # Mindspore
    parser.add_argument('--device_id', type=int, default=1)
    parser.add_argument('--device_target', type=str, default='GPU')

    # Export
    parser.add_argument('--export_model', type=str)

    ##load default config file (dataset, model, optimization)
    args_config, remaining = config_parser.parse_known_args()

    ## read config files
    opt = yaml.safe_load(open(args_config.data_config, "r"))
    parser.set_defaults(**opt)

    ## load remaining args
    args = parser.parse_args(remaining)
    args = check_folders(args)
    return args

def compute_adjustment(train_loader, tro, args):
    label_freq = {}
    for _, (_, _, _, target) in enumerate(train_loader):
        target = target.to(args.gpu)
        for j in target:
            key = int(j.item())
            label_freq[key] = label_freq.get(key, 0) + 1
    label_freq = dict(sorted(label_freq.items()))
    label_freq_array = np.array(list(label_freq.values()))
    label_freq_array = label_freq_array / label_freq_array.sum()
    adjustments = np.log(label_freq_array ** tro + 1e-12)
    adjustments = adjustments.asnumpy()
    adjustments = adjustments.astype(mindspore.float16)
    return adjustments


def calc_confusion_mat(val_loader, model, args):

    model.eval()
    all_preds = []
    all_targets = []
    for _, (inputs, target) in enumerate(val_loader):
        # compute output
        output = model(inputs)
        _, pred = ops.ArgMaxWithValue(axis=1)(output)
        all_preds.extend(pred.asnumpy())
        all_targets.extend(target.asnumpy())
    cf = confusion_matrix(all_targets, all_preds).astype(float)

    cls_cnt = cf.sum(axis=1)
    cls_hit = np.diag(cf)
    cls_acc = cls_hit / cls_cnt

    print('Class Accuracy : ')
    print(cls_acc)
    classes = [str(x) for x in args.cls_num_list]
    plot_confusion_matrix(all_targets, all_preds, classes)
    plt.savefig(os.path.join(args.root_log, args.store_name, 'confusion_matrix.png'))

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):

    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax


def check_folders(args):
    def sanity_check(folder, create_flag=False, is_file=False):

        if is_file is True:
            if os.path.exists(folder):
                print("file {} already exists. ".format(folder))
                return is_file
            folder = os.path.dirname(folder)

        exist_flag = os.path.exists(folder)
        if exist_flag is True:
            print("folder {} already exists. ".format(folder))
        else:
            print("folder {} does not exists. ".format(folder))
            if create_flag is True:
                os.makedirs(folder)
                print('creating folder {} .........done.'.format(folder))
        return is_file

    ## check folder and create if needed
    sanity_check(args.data_folder) #
    sanity_check(args.log_folder, create_flag=True) #
    sanity_check(args.checkpoint_folder, create_flag=True,) #

    ## check files
    sanity_check(args.excel, create_flag=True, is_file=True)
    sanity_check(args.resume_checkpoint, create_flag=True, is_file=True) # if not exist then create folder

    ## create store_name
    args.store_name = "_".join([args.data_name, str(args.data_ratio),
                                args.arch, "pretrained", str(args.pretrained), "resume", str(args.resume),
                                "bnm", str(args.bn_momentum), "batch_size", str(args.batch_size),
                                "epochs", str(args.epochs), "k", str(args.k), "exp", str(args.comment)])

    sanity_check(os.path.join(args.log_folder, args.store_name), create_flag=True)
    sanity_check(os.path.join(args.checkpoint_folder, args.store_name), create_flag=True)

    return args

def save_checkpoint(args, state, is_best):

    filename = os.path.join(args.checkpoint_folder, args.store_name, "best.ckpt")
    if is_best:
        mindspore.save_checkpoint(state, filename)

def save_features(args, features, labels, train_features, train_labels, epoch):
    if features:
        features = np.concatenate((features, labels), axis=1)
        filename = os.path.join(args.checkpoint_folder, args.store_name, "features_"+str(epoch)+".npy")
        np.save(filename, features)

    if train_features:
        train_features = np.concatenate((train_features, train_labels), axis=1)
        filename = os.path.join(args.checkpoint_folder, args.store_name, "train_features_"+str(epoch)+".npy")
        np.save(filename, train_features)


def dump_to_excel(args, best_acc1=0):

    opt = vars(args)
    opt.update({"RESULTS": best_acc1})
    data = opt.fromkeys([
            'data_name', 'data_ratio', 'arch', 'bn_momentum', 'pretrained',
            'resume', 'batch_size', 'epochs', 'drop_last',
            'optim_type', 'base_lr', 'lsr', 'warm_up', 'weight_decay',
            'momentum', 'k', 'loss_type', 'RESULTS'])

    key, val = zip(*data.items())
    val = np.array(val).reshape(1, -1)

    if not os.path.exists(args.excel):
        frame = pd.DataFrame(val, columns=key)
        frame.to_excel(args.excel, index=False)
        return

    old_frame = pd.read_excel(args.excel)

    out = np.vstack([old_frame.values, val])
    frame = pd.DataFrame(out, columns=key)
    frame.to_excel(args.excel, index=False)

class AverageMeter:

    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)


def accuracy(output, target, topk=(1,)):
    batch_size = target.shape[0]
    _, pred = ops.top_k(output, 1, True)
    # _, pred = output.topk(maxk, 1, True, True)
    pred = ops.transpose(pred, (1, 0))
    correct = ops.equal(pred, target.astype(mindspore.int32).view(1, -1).expand_as(pred))

    res = []
    for k in topk:
        correct_k = correct[:k].reshape(-1).astype(mindspore.float32).sum(0, keepdims=True)
        res.append(ops.mul(correct_k, 100.0 / batch_size))
    return res
