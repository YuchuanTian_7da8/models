import mindspore.ops as ops
import mindspore.common.initializer as init
from mindspore import Tensor
from mindspore import Parameter
import mindspore.nn as nn
from .ltbn import TwoInputSequential, TwoConv2d
from .gmm_resnet import MyBatchNorm2d
from .model_registry import register_model

__all__ = ['ResNet_s', 'resnet20', 'resnet32', 'resnet44', 'resnet56', 'resnet110', 'resnet1202']

def _weights_init(m):
    if isinstance(m, (nn.Dense, nn.Conv2d)):
        init.HeNormal(m.weight)

class NormedLinear(nn.Cell):
    def __init__(self, in_features, out_features):
        super(NormedLinear, self).__init__()
        self.weight = Parameter(Tensor(in_features, out_features))
        self.weight.data.uniform_(-1, 1).renorm_(2, 1, 1e-5).mul_(1e5)

    def construct(self, x):
        out = ops.MatMul(ops.L2Normalize(axis=1)(x), ops.L2Normalize(axis=0)(self.weight))
        return out

class LambdaLayer(nn.Cell):
    def __init__(self, planes):
        super(LambdaLayer, self).__init__()
        self.planes = planes
        self.pad = nn.Pad(paddings=((0, 0), \
            (self.planes//4, self.planes//4), (0, 0), (0, 0)), mode="CONSTANT")

    def construct(self, x):
        return self.pad(x[:, :, ::2, ::2])

def NormedLayer(channel=32, momentum=0.5, sync=False, num_class=10, k=5):
    return MyBatchNorm2d(num_features=channel, num_mixtures=k, momentum=momentum)

class BasicBlock(nn.Cell):
    expansion = 1
    def __init__(self, in_planes, planes, stride=1, option='A', sync=False, bnm=0.5, num_class=10, k=5):
        super(BasicBlock, self).__init__()
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=3, stride=stride, pad_mode='pad', padding=1)
        self.bn1 = NormedLayer(planes, bnm, sync, num_class, k)

        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=1, pad_mode='pad', padding=1)
        self.bn2 = NormedLayer(planes, bnm, sync, num_class, k)
        self.option = option
        self.shortcut = nn.SequentialCell()
        self.relu = ops.ReLU()
        if stride != 1 or in_planes != planes:
            if option == 'A':
                self.shortcut = LambdaLayer(planes)
            elif option == 'B':
                self.shortcut = TwoInputSequential(TwoConv2d(in_planes, self.expansion*planes, \
                    kernel_size=1, stride=stride, bias=False), \
                    NormedLayer(self.expansion*planes, bnm, sync, num_class, k))

    def construct(self, x, idx):
        out = self.conv1(x)
        out, _ = self.bn1(out, idx)
        out = self.relu(out)
        out = self.conv2(out)
        out, _ = self.bn2(out, idx)

        if self.option == "B":
            tmp, _ = self.shortcut(x, idx)
        else:
            tmp = self.shortcut(x)
        out += tmp
        out = self.relu(out)
        return out, idx

@register_model
class ResNet_s(nn.Cell):

    def __init__(self, block, num_blocks, num_classes=10, \
                 use_norm=False, bnm=0.5, sync=False, channel=3, k=5):
        super(ResNet_s, self).__init__()
        self.k = k
        self.num_class = num_classes
        self.in_planes = 16
        self.bnm = bnm
        self.sync = sync
        self.conv1 = nn.Conv2d(channel, 16, kernel_size=3, stride=1, pad_mode='pad', padding=1)
        self.bn1 = NormedLayer(channel=16, momentum=bnm, sync=sync, num_class=num_classes, k=k)

        self.layer1 = self._make_layer(block, 16, num_blocks[0], stride=1)
        self.layer2 = self._make_layer(block, 32, num_blocks[1], stride=2)
        self.layer3 = self._make_layer(block, 64, num_blocks[2], stride=2)
        self.relu = ops.ReLU()

        out_dim = 64

        self.linear = nn.Dense(out_dim, num_classes)
        # 递归初始化权重
        # self.apply(_weights_init)
        for m in self.cells():
            _weights_init(m)


    def _make_layer(self, block, planes, num_blocks, stride):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for s in strides:
            layers.append(block(self.in_planes, planes, s, bnm=self.bnm, \
                                sync=self.sync, num_class=self.num_class, k=self.k))
            self.in_planes = planes * block.expansion

        return TwoInputSequential(*layers)

    def construct(self, x, idx=None):
        out = self.conv1(x)
        out, _ = self.bn1(out, idx)

        out = self.relu(out)

        out, _ = self.layer1(out, idx)
        out, _ = self.layer2(out, idx)
        out, _ = self.layer3(out, idx)
        out = ops.avg_pool2d(out, out.shape[3])
        feat = out.view(out.shape[0], -1)

        out = self.linear(feat)

        return out


@register_model
def resnet20(args):
    num_classes = args.num_class if hasattr(args, 'num_class') else 10
    use_norm = args.use_norm if hasattr(args, 'use_norm') else False
    channel = args.input_ch if hasattr(args, 'input_ch') else 3
    bnm = args.bn_momentum if hasattr(args, 'bn_momentum') else 0.1
    sync = args.distributed if hasattr(args, 'distributed') else False
    k = args.k if hasattr(args, 'k') else 1
    return ResNet_s(BasicBlock, [3, 3, 3], num_classes=num_classes, \
                    use_norm=use_norm, bnm=bnm, channel=channel, sync=sync, k=k)

@register_model
def resnet32(args):
    num_classes = args.num_class if hasattr(args, 'num_class') else 10
    use_norm = args.use_norm if hasattr(args, 'use_norm') else False
    channel = args.input_ch if hasattr(args, 'input_ch') else 3
    bnm = args.bn_momentum if hasattr(args, 'bn_momentum') else 0.1
    sync = args.distributed if hasattr(args, 'distributed') else False
    k = args.k if hasattr(args, 'k') else 1
    return ResNet_s(BasicBlock, [5, 5, 5], num_classes=num_classes, use_norm=use_norm, \
                    bnm=bnm, channel=channel, sync=sync, k=k)

@register_model
def resnet44(args):
    num_classes = args.num_class if hasattr(args, 'num_class') else 10
    use_norm = args.use_norm if hasattr(args, 'use_norm') else False
    channel = args.input_ch if hasattr(args, 'input_ch') else 3
    bnm = args.bn_momentum if hasattr(args, 'bn_momentum') else 0.1
    sync = args.distributed if hasattr(args, 'distributed') else False
    k = args.k if hasattr(args, 'k') else 1
    return ResNet_s(BasicBlock, [7, 7, 7], num_classes=num_classes, use_norm=use_norm, \
                    bnm=bnm, channel=channel, sync=sync, k=k)

@register_model
def resnet56(args):
    num_classes = args.num_class if hasattr(args, 'num_class') else 10
    use_norm = args.use_norm if hasattr(args, 'use_norm') else False
    channel = args.input_ch if hasattr(args, 'input_ch') else 3
    bnm = args.bn_momentum if hasattr(args, 'bn_momentum') else 0.1
    sync = args.distributed if hasattr(args, 'distributed') else False
    k = args.k if hasattr(args, 'k') else 1
    return ResNet_s(BasicBlock, [9, 9, 9], num_classes=num_classes, use_norm=use_norm, \
                    bnm=bnm, channel=channel, sync=sync, k=k)

@register_model
def resnet110(args):
    num_classes = args.num_class if hasattr(args, 'num_class') else 10
    use_norm = args.use_norm if hasattr(args, 'use_norm') else False
    channel = args.input_ch if hasattr(args, 'input_ch') else 3
    bnm = args.bn_momentum if hasattr(args, 'bn_momentum') else 0.1
    sync = args.distributed if hasattr(args, 'distributed') else False
    k = args.k if hasattr(args, 'k') else 1
    return ResNet_s(BasicBlock, [18, 18, 18], num_classes=num_classes, use_norm=use_norm, \
                    bnm=bnm, channel=channel, sync=sync, k=k)

@register_model
def resnet1202(args):
    num_classes = args.num_class if hasattr(args, 'num_class') else 10
    use_norm = args.use_norm if hasattr(args, 'use_norm') else False
    channel = args.input_ch if hasattr(args, 'input_ch') else 3
    bnm = args.bn_momentum if hasattr(args, 'bn_momentum') else 0.1
    sync = args.distributed if hasattr(args, 'distributed') else False
    k = args.k if hasattr(args, 'k') else 1
    return ResNet_s(BasicBlock, [200, 200, 200], num_classes=num_classes, use_norm=use_norm, \
                    bnm=bnm, channel=channel, sync=sync, k=k)

def test(net):
    import numpy as np
    total_params = 0

    for x in filter(lambda p: p.requires_grad, net.parameters()):
        total_params += np.prod(x.data.numpy().shape)
    print("Total number of params", total_params)
    print("Total layers", len(list(filter(lambda p: p.requires_grad and len(p.data.size()) > 1, net.parameters()))))


if __name__ == "__main__":
    for net_name in __all__:
        if net_name.startswith('resnet'):
            print(net_name)
            test(globals()[net_name]())
            print()
