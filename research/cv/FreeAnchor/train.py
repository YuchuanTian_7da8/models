# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Train the model"""
import argparse
import os
from pathlib import Path
from pprint import pprint
import json

import numpy as np
from tqdm import tqdm

import mindspore as ms
from mindspore import Tensor
from mindspore.train import Model
from mindspore.nn import Adam
from mindspore.train.callback import TimeMonitor

from src.data.config import (
    parse_yaml, parse_cli_to_yaml, merge, Config, compute_features_info
)
from src.data.dataset import prepare_data
from src.models.freeanchor.freeanchor import build_model
from src.models.freeanchor.layers.init_params import init_net_param
from src.models.freeanchor.layers.retinanet import (
    RetinanetWithLossCell, RetinanetInferWithDecoder
)
import src.tools.callbacks as cb_fn
from src.tools.lr_schedule import multistep_lr
from src.tools.cell import cast_amp

from pycocotools.cocoeval import COCOeval
from pycocotools.coco import COCO


def get_config():
    """
    Get Config according to the yaml file and cli arguments.
    """
    parser = argparse.ArgumentParser(description='default name',
                                     add_help=False)
    current_dir = os.path.dirname(os.path.abspath(__file__))
    parser.add_argument('--config_path', type=str,
                        default=os.path.join(current_dir,
                                             'default_config.yaml'),
                        help='Config file path')
    parser.add_argument('--checkpoint_path', help='Path to save checkpoint.')
    parser.add_argument('--eval_results_path', default='eval_results',
                        help='Path to folder with evaluation results.')
    parser.add_argument('--prediction_path', help='Path to model predictions.')
    path_args, _ = parser.parse_known_args()
    default, helper, choices = parse_yaml(path_args.config_path)
    args = parse_cli_to_yaml(parser=parser, cfg=default, helper=helper,
                             choices=choices, cfg_path=path_args.config_path)
    final_config = Config(merge(args, default))
    final_config = compute_features_info(final_config)
    pprint(final_config)
    print('Please check the above information for the configurations',
          flush=True)
    return final_config


def apply_nms(all_boxes, all_scores, thres, max_boxes):
    """Apply NMS to bboxes."""
    y1 = all_boxes[:, 0]
    x1 = all_boxes[:, 1]
    y2 = all_boxes[:, 2]
    x2 = all_boxes[:, 3]
    areas = (x2 - x1 + 1) * (y2 - y1 + 1)

    order = all_scores.argsort()[::-1]
    keep = []

    while order.size > 0:
        i = order[0]
        keep.append(i)

        if len(keep) >= max_boxes:
            break

        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h

        ovr = inter / (areas[i] + areas[order[1:]] - inter)

        inds = np.where(ovr <= thres)[0]

        order = order[inds + 1]
    return keep


def main():
    config = get_config()

    ms.set_context(mode=config.pynative_mode,
                   device_target=config.device_target)

    train_ds, val_ds = prepare_data(config)
    train_dataset_size = train_ds.get_dataset_size()
    model = build_model(config)
    net = RetinanetWithLossCell(model, config)
    cast_amp(net, config)
    eval_net = RetinanetInferWithDecoder(model, config)
    init_net_param(net)

    lr = Tensor(multistep_lr(config, train_dataset_size), ms.float32)
    cb = [
        TimeMonitor(),
        cb_fn.EvalCocoCallback(Path('./ckpt')),
        cb_fn.SummaryCallbackWithEval('./summary', 10),
    ]
    opt = Adam(params=net.trainable_params(), learning_rate=lr,
               weight_decay=config.weight_decay)
    model = Model(net, optimizer=opt,)

    val_cls_dict = {i: cls for i, cls in enumerate(config.coco_classes)}
    coco_gt = COCO(config.val_dataset_ann)
    classs_dict = {cat["name"]: cat["id"]
                   for cat in coco_gt.loadCats(coco_gt.getCatIds())}

    print('Model is ready')
    model.train(config.epochs, train_ds, callbacks=cb, dataset_sink_mode=True)
    img_ids, predictions = [], []
    for data in tqdm(val_ds.create_dict_iterator(output_numpy=True),
                     total=val_ds.get_dataset_size()):
        img_np = data['image']
        batch_size = img_np.shape[0]
        img_id = data['image_shape'][0, -batch_size:]
        image_shape = data['image_shape']

        output = eval_net(Tensor(img_np))
        for batch_idx in range(batch_size):
            pred_boxes = output[0].asnumpy()[batch_idx]
            box_scores = output[1].asnumpy()[batch_idx]
            img_id = int(np.squeeze(img_id[batch_idx]))
            h, w = image_shape[batch_idx][:2]

            final_boxes, final_label, final_score = [], [], []
            img_ids.append(img_id)

            for c in range(1, config.num_classes):
                class_box_scores = box_scores[:, c]
                score_mask = class_box_scores > config.min_score
                class_box_scores = class_box_scores[score_mask]
                class_boxes = pred_boxes[score_mask] * [h, w, h, w]

                if score_mask.any():
                    nms_index = apply_nms(
                        class_boxes, class_box_scores,
                        config.nms_thershold, config.max_boxes
                    )
                    class_boxes = class_boxes[nms_index]
                    class_box_scores = class_box_scores[nms_index]
                    final_boxes += class_boxes.tolist()
                    final_score += class_box_scores.tolist()
                    final_label += \
                        [classs_dict[val_cls_dict[c]]] * len(class_box_scores)
            for loc, label, score in zip(
                    final_boxes, final_label, final_score
            ):
                predictions.append({
                    'image_id': img_id,
                    'bbox': [loc[1], loc[0], loc[3] - loc[1], loc[2] - loc[0]],
                    'score': score,
                    'category_id': label
                })

    if not predictions:  # dummy prediction for COCO tools
        predictions.append({
            'image_id': int(img_id[0]),
            'bbox': [0, 0, 0, 0],
            'score': 0.0,
            'category_id': 'background'
        })
    with open('predictions.json', 'w') as f:
        json.dump(predictions, f)

    coco_dt = coco_gt.loadRes('predictions.json')
    E = COCOeval(coco_gt, coco_dt, iouType='bbox')
    E.params.imgIds = img_ids
    E.evaluate()
    E.accumulate()
    E.summarize()
    mAP = E.stats[0]
    print("\n========================================\n")
    print(f"mAP: {mAP}")


if __name__ == '__main__':
    main()
