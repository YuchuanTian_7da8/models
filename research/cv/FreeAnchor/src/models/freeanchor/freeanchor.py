# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Initialize FreeAnchor (RetinaNet) model, pass architecture parameters."""
from .layers.retinanet import RetinaNet50
from .layers.backbone import resnet, resnext


def build_model(args):
    if args.backbone.type == "ResNeXt":
        backbone = resnext.ResNeXt(depth=args.backbone.depth,
                                   groups=args.backbone.groups,
                                   base_width=args.backbone.base_width,
                                   num_stages=args.backbone.num_stages,
                                   out_indices=args.backbone.out_indices,
                                   frozen_stages=args.backbone.frozen_stages)
    else:
        backbone = resnet.ResNet(depth=args.backbone.depth)
    model = RetinaNet50(backbone, args)
    return model
