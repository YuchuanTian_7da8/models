# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""FSAF"""
import numpy as np
import mindspore as ms
from mindspore import nn
from mindspore.common.tensor import Tensor

from .fsaf_head import FSAFHead
from .resnet import ResNet
from .fpn import FPN


class FSAF(nn.Cell):
    """
    FSAF Network.

    Examples:
        net = FSAF(config)
    """

    def __init__(self, config):
        super().__init__()
        self.dtype = np.float32
        self.ms_type = ms.float32
        self.train_batch_size = config.batch_size
        self.test_batch_size = config.test_batch_size
        self.num_classes = config.num_classes

        # Anchor generator
        self.gt_labels_stage1 = Tensor(
            np.ones((self.train_batch_size, config.num_gts)).astype(np.uint8)
        )

        # Backbone
        self.backbone = self.create_backbone(config.backbone)

        # Neck
        self.neck = FPN(in_channels=config.neck.fpn.in_channels,
                        out_channels=config.neck.fpn.out_channels,
                        num_outs=config.neck.fpn.num_outs,
                        start_level=config.neck.fpn.start_level,
                        feature_shapes=config.feature_shapes,
                        add_extra_convs=config.neck.fpn.add_extra_convs)

        self.bbox_head = FSAFHead(config=config.bbox_head,
                                  train_cfg=config.train_cfg,
                                  test_cfg=config.test_cfg)

    @staticmethod
    def create_backbone(config):
        """Create backbone and init it."""
        backbone = ResNet(
            depth=config.depth,
            num_stages=config.num_stages,
            strides=(1, 2, 2, 2),
            dilations=(1, 1, 1, 1),
            out_indices=config.out_indices,
            frozen_stages=config.frozen_stages,
            norm_eval=config.norm_eval
        )

        new_param = {}
        param_dict = ms.load_checkpoint(config.pretrained)

        for key, value in param_dict.items():
            if key.startswith('res_layers.'):
                new_param[key.replace("res_layers.", "0.")] = value
            else:
                new_param[key] = value

        ms.load_param_into_net(backbone, new_param)
        return backbone

    def construct(self, img_data, img_metas, gt_bboxes, gt_labels, gt_valids):
        """
        construct the LibraRcnn Network.

        Args:
            img_data: input image data.
            img_metas: meta label of img.
            gt_bboxes (Tensor): get the value of bboxes.
            gt_labels (Tensor): get the value of labels.
            gt_valids (Tensor): get the valid part of bboxes.

        Returns:
            Tuple,tuple of output tensor (losses if training else predictions).
        """
        x = self.backbone(img_data)
        x = self.neck(x)
        bbox_pred, cls_score = self.bbox_head(x)

        if self.training:
            losses_cls, losses_bbox = self.bbox_head.loss(
                cls_scores=cls_score, bbox_preds=bbox_pred,
                gt_valids=gt_valids, gt_bboxes=gt_bboxes,
                gt_labels=gt_labels, img_metas=img_metas)
            return losses_cls, losses_bbox
        return bbox_pred, cls_score

    def set_train(self, mode=True):
        """Change training mode."""
        super().set_train(mode=mode)
        self.backbone.set_train(mode=mode)


class FSAFInfer:
    """LibraRCNN wrapper for inference."""

    def __init__(self, net):
        super().__init__()
        self.net = net
        self.net.set_train(False)

    def construct(self, img_data, img_metas=None):
        """Make predictions."""
        bbox_preds, cls_scores = self.net(img_data, img_metas,
                                          None, None, None)

        return bbox_preds, cls_scores

    def __call__(self, *args, **kwargs):
        return self.construct(*args, **kwargs)
